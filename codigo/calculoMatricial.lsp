;ALGUMAS CORRECOES DEVEM SER FEITAS NESTE ARQUIVO, ELE FOI O PRIMEIRO QUE ESCREVI, APOS ELE JA APREENDI NOVAS FORMAS DE FAZER AS COISAS
;============================================================================================================================================================
;essa funcao imprime uma patriz no autocad, nao tem nenhum uso, mas � boa debugar os programas que usam matrizes
(defun princmat(m / contadori contadorj i j )	
	;pegar linhas e colunas
	(setq i 0)
	(setq j 0)	
	(while (/= (nth i m) nil)
		(setq i (+ i 1)); aumentar  o contador ate o final das linhas
	)
	(while (/= (nth j (car m)) nil)
		(setq j (+ j 1)); aumentar  o contador ate o final das colunas
	)
	(princ "+++++++++++++++++++++")
	(princ "\ndimensao: ")
	(princ i)
	(princ "x")
	(princ j)
 	(princ "\n")
 	(princ "------------\n")
	
	; imprime os elementos da matriz
	(setq contadori 0)
	(while (< contadori i) 
		(setq contadorj 0)
		(princ "|")
		(while (< contadorj j)
			(princ " ")
			(princ (gettermo contadori contadorj m))
			(setq contadorj (+ contadorj 1))
		)
		(princ " |")		
		(princ "\n")
		(setq contadori (+ contadori 1))
	)
	(princ "+++++++++++++++++++++")
	(princ)
)
;============================================================================================================================================================
; essa funcao recebe duas matrizes, lista de lista, e retorna a mutiplica�ao delas ou uma mensagem de que nao � possivel realizar a multiplicaao
(defun multiplicaMat(A B / C contadori contadorj Ai Aj Bi Bj contadork resultado termo linha)
	(setq A (transformaemfloat A));coloca as matrizes em float pra nao ter probkemas de arredondamento
	(setq B (transformaemfloat B));coloca as matrizes em float pra nao ter probkemas de arredondamento
	;pegar linhas e colunas de A
	(setq Ai 0)
	(setq Aj 0)	
	(while (/= (nth Ai A) nil)	(setq Ai (+ Ai 1)))
	(while (/= (nth Aj (car A)) nil)  (setq Aj (+ Aj 1)))
	;pegar linhas e colunas de B
	(setq Bi 0)
	(setq Bj 0)	
	(while (/= (nth Bi B) nil)	(setq Bi (+ Bi 1)))
	(while (/= (nth Bj (car B)) nil)  (setq Bj (+ Bj 1)))
	
	(if (= Aj Bi); a mutiplicacao so � possivel se as colunas de A forem compatives com as linhas de B
		(progn
			;como nao se cria uma matriz, aqui fui fazendo uma linha de cada vez de depois adiconando a 'matriz'
			(setq resultado (list))
			(setq contadori (- Ai 1))
			(while (>= contadori 0)
				(setq linha (list))
				(setq contadorj 0)
				(while (< contadorj Bj)
					(setq termo 0)
					(setq contadork 0)
					(while (< contadork Bi)
						(setq termo (+ termo (* (gettermo contadori contadork A) (gettermo contadork contadorj B))))
						(setq contadork (+ contadork 1))
					)
					(setq linha (append linha (list termo)))
					(setq contadorj (+ contadorj 1))
				)	
				(setq resultado (cons linha resultado))
				(setq contadori (- contadori 1))
			)
			(setq C resultado); saida da funcao		
		)
		(progn
			(princ "nao permitido, dimensoes devem ser compativeis")
			(princ)
		)
	)
)
;============================================================================================================================================================
; esse codigo inverte uma matriz, que deve ser quadrada,;
; sei que nao �e a forma mais rapida computacionalmente de resolver um sistema linear, mas � a que eu conheco
(defun inverteMat(entrada / i j saida contadork con contadorj contadori con)
	;pegar linhas e colunas de M
	(setq m (transformaemfloat entrada))
	(setq i 0)
	(setq j 0)	
	(while (/= (nth i M) nil) (setq i (+ i 1)))
	(while (/= (nth j (car M)) nil) (setq j (+ j 1)))
	(if (= i j)
		(progn
			(setq contadork 0)
			(while (< contadork i)
				(setq con (gettermo contadork contadork M))
				(setq m (settermo m contadork contadork 1))
				(setq contadorj 0)
				(while (< contadorj i) 
					(setq m (settermo m contadork contadorj (/ (gettermo contadork contadorj m) con)))
					(setq contadorj (+ contadorj 1))
				)
				(setq contadori 0)
				(while (< contadori i)
					(if (/= contadori contadork)
					(progn
						(setq con (gettermo contadori contadork m))
						(setq m (settermo m contadori contadork 0))
						(setq contadorj 0)
						(while (< contadorj i)
							(setq m (settermo m contadori contadorj (- (gettermo contadori contadorj m) (* con (gettermo contadork contadorj m)))))
							(setq contadorj (+ contadorj 1))
						)
					)
					(progn
						(princ)
					)
					)
					(setq contadori (+ contadori 1))
				)
			(setq contadork (+ contadork 1))
			)
			(setq saida m)
			)
		(progn 
			(princ "nao pode, a matriz precisa ser quadrada")
		)
	)		
)
;============================================================================================================================================================
;forma em que estou resolvendo um sitema linear, 
;mutiplica a inversa dos coeficientes pela mat dos termos independentes
;a saida � a matriz coluna com o vetor resposta 
(defun resolveSistema(A B / saida)
	(setq saida (multiplicaMat (inverteMat A) B))
)
;============================================================================================================================================================
;funcao para passar uma matriz de inteiro para float
(defun transformaemfloat(m / i  j contadori contadorj saida)
	(setq saida m)
	(setq i 0)
	(setq j 0)	
	(while (/= (nth i M) nil) (setq i (+ i 1)))
	(while (/= (nth j (car M)) nil) (setq j (+ j 1)))
	(setq contadori 0)
	(while (< contadori i)
		(setq contadorj 0)
		(while (< contadorj j)
			(setq saida (settermo saida contadori contadorj (float (gettermo contadori contadorj saida))))
			(setq contadorj (+ contadorj 1))
		)
		(setq contadori (+ contadori 1))
	)
	(setq saida saida)
)
;============================================================================================================================================================
;funcao para pegar um termo especifico de uma matriz
(defun gettermo(i j m / r linha)
	(setq linha (nth i m));escolhe a linha
	(setq r (nth j linha));escolhe a coluna
)
;============================================================================================================================================================
;funcao que altera um termo especifico de uma matriz
(defun settermo(m i j valor / contadori contadorj linha saida auxiliar Mi Mj); matriz , 1 dim, 2 dim, valor a ser substituido
	;pegar linhas e colunas de M
	(setq Mi 0)
	(setq Mj 0)	
	(while (/= (nth Mi M) nil) (setq Mi (+ Mi 1)))
	(while (/= (nth Mj (car M)) nil) (setq Mj (+ Mj 1)))
	;-----------
	(setq auxiliar (list)); lista vazia
	(setq contadori (- Mi 1))
	(while (>= contadori 0)
		(setq linha (list))
		(setq contadorj 0)			
		(while (< contadorj Mj)
			(if (and (= contadori i) (= contadorj j))
				(progn
					(setq linha (append linha (list valor)))
				)
				(progn
					(setq linha (append linha (list (gettermo contadori contadorj m))))
				)				
			)
		;(setq linha (append linha (list 0)))
		(setq contadorj (+ contadorj 1))
		)
	(setq auxiliar (cons linha auxiliar))
	(setq contadori (- contadori 1))
	)		
	(setq saida auxiliar)
)
;============================================================================================================================================================