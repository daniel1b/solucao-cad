corrigeInstalar : dialog {
label = "Elemento Poste";
	: column{
		: edit_box {
				key = "sequencia";
				edit_width = 10;
				fixed_width = true ;
				Value = "";
				alignment = centered;
		}
		: button {
			edit_width = 10;
			fixed_width = true ;
            key = "ultimo";
            label = " Ultimo";
			alignment = centered;
        }
	}
 : row {
		: column { 
			: boxed_column {
				label = "Tipo: ";
				: radio_column {
					key = "poste";
					: radio_button {
						key = "dt";
						label = "poste dt";
					}
					: radio_button {
						key = "cc";
						label = "poste cc";
					}
				}
			}
			:boxed_column{
				label = "tam/res";
				: edit_box {
					key = "resistenciatamanho";
					edit_width = 10;
					fixed_width = true ;
					Value = "";
					alignment = left;
				}
			}			
		}
		: column { 
			:boxed_column{
				label = "Baixa";
				: edit_box {					
					key = "estruturaBT";
					edit_width = 10;
					fixed_width = true ;
					Value = "";
					alignment = left;
				}
			
			}
			:boxed_column{
				label = "Media";
				: edit_box {					
					key = "estruturaMT";
					edit_width = 10;
					fixed_width = true ;
					Value = "";
					alignment = left;
				}
			
			}
			:boxed_column{		
				label = "ilmuminacao";
					: edit_box {
						key = "iluminacao";
						edit_width = 5;
						fixed_width = true ;
						Value = "";
						alignment = left;
					}
					: text {
						key = "ilmunicaoTexto";
						value = "x10W";
						alignment = left;
					}	
			}
		}
		: boxed_column {			
			 label = "Codigo";
			: edit_box {
				key = "posto";
				edit_width = 20;
				fixed_width = true ;
				Value = "";
				alignment = left;
			}	
			: toggle { 
                  key = "trafo"; 
                   label = "Trafo"; 
             }
			 : toggle { 
                  key = "fusivel"; 
                   label = "Chave Fus."; 
             }
			 : toggle { 
                  key = "aterramento"; 
                   label = "Aterramento"; 
             }
			 : toggle { 
                  key = "pararaio"; 
                   label = "Para-raio"; 
             }
		}
	}
		: boxed_row {
              : button {
                key = "accept";
                label = " Ok ";
                is_default = true;
              }
              : button {
                key = "cancel";
                label = " Cancel ";
                is_default = false;
                is_cancel = true;
              }
         }	
 }





