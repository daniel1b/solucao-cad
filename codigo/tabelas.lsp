;===================================== ======================================
(defun insereTabelaPoste (Ei adesenhar / id txt1 txt2 txt3 txt4 layeratual ponto grr lasttabela)
	(setq ponto '(0 0 0))
	(setq id (cadr (assoc 97 adesenhar)))
	(setq txt1 (cadr (assoc 99 adesenhar)))
	(setq txt2 (cadr (assoc 100 adesenhar)))
	(setq txt3 (cadr (assoc 102 adesenhar)))
	(setq txt4 (cadr (assoc 101 adesenhar)))
	
	(setq layeratual (getvar "clayer"))
	(setlayer g_layerTabelas)
	
	(if	(and TXT4 TXT2)
		(progn			
			(command "_insert" "ID_POSTE4" ponto "1" "1" "0" TXT4 TXT1 TXT3 id TXT2)			
		)
		(progn
			(if (or TXT4 TXT2)
				(progn
					(if txt4
						(command "_insert" "ID_POSTE3" ponto "1" "1" "0" TXT4 TXT1 TXT3 id)
						(command "_insert" "ID_POSTE3" ponto "1" "1" "0" TXT2 TXT1 TXT3 id)
					)
				)
				(command "_insert" "ID_POSTE2" ponto "1" "1" "0" TXT1 TXT3 id)		
			)
		)
	)
	(setq lasttabela (entlast))
	(setvar "clayer" layeratual)
	(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
		(setq ponto (cadr grr))
		(redraw)
		(moveTabela lasttabela ponto)
	)
	
	;(editartabela lasttabela)

)
;===================================== ======================================
(defun insereTabelaPosto (Ei adesenhar / ponto grr lasttabela)
	(if (cdr(assoc 106 adesenhar))
		(progn
			(setq id (cadr (assoc 97 adesenhar)))
			(setq txt1 (nth 0 (cdr (assoc 106 adesenhar))))
			(setq txt2 (nth 1 (cdr (assoc 106 adesenhar))))
			(setq txt3 (nth 2 (cdr (assoc 106 adesenhar))))
			(setq ponto '(0 0 0))
			(setq layeratual (getvar "clayer"))
			(setlayer g_layerTabelas)
			(command "_insert" "ID_TRAFO3" ponto "1" "1" "0" TXT1 TXT2 TXT3 ID)
			(setq lasttabela (entlast))
			(setvar "clayer" layeratual)
			(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
						(setq ponto (cadr grr))
						(redraw)
						(moveTabela lasttabela ponto)
			)
			
			;(editartabela lasttabela)
		)
	)
)
;=================================================================================
(defun editartabela (selecao / p1 p2 osnapvar)
(while (or (initget "Continuar Mover Alinhar") (/= "Continuar" (setq resposta (cond ( (getkword "\nEnter p/ continuar... [Continuar/Mover/Alinhar] <Continuar>: ") ) ( "Continuar" )))))
	(if  (= "Alinhar" resposta)
		(progn 
			(alinha selecao)
		)
		(progn
			(princ "\nja alinhado.")
		)
	)
	
	(if  (= "Mover" resposta)
		(progn 
			(setq p1 (getpoint "\nponto de referencia: "))
			(setq p2 (getpoint p1 "\nponto final: "))
			(setq osnapvar (getvar "osmode"))
			(setvar "osmode" 0)
			(command "move" selecao "" p1 p2)
			(setvar "osmode" osnapvar)
		)
		(progn
			(princ "\nja no lugar correto.")
		)
	)
)
)
;=================================================================================
(defun moveTabela (tabelanome ponto / vetorDeMover pontoincersaoB pontoincersaoE aux)
	(setq att tabelanome)
	(setq aux (entget att))	
	(setq pontoincersaoB(cdr(assoc 10 aux)))
	(setq aux (subst (cons 10 ponto) (assoc 10 aux) aux))
	(entmod aux)	
	(setq vetorDeMover (diferencaPontos ponto pontoincersaoB))
	(while (setq pontoincersaoE(cdr(assoc 11 (setq aux (entget (setq att (entnext att)))))))
		(setq pontoincersaoE (somaPontos vetorDeMover pontoincersaoE))
		(setq aux (subst (cons 11 pontoincersaoE) (assoc 11 aux) aux))
		(entmod aux)
	)
	(princ)
)
;=================================================================================
















