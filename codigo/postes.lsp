;==================================================================================================================================================================
;funcao que posiciona um bloco de poste DT alinhado a uma linha
(defun posicionaDTalinhado(postenome linha / vetor incpt ptlinha1 ptlinha2 a b ponto grr numpi)
	(setq numpi 3.14159265359)
	(setq ponto '(0 0 0));ponto inicial do poste - acredito que seja um problema, mas nao consegui pensar em um melhor
	
	;pegar os pontos de inicio e fim da linha
	(setq linha (entget linha))
	(setq ptlinha1 (cdr (assoc 10 linha)))
	(setq ptlinha2 (cdr (assoc 11 linha)))	
	
	;girar o poste no angulo da linha
	(girarTudo postenome (atan (- (cadr ptlinha1) (cadr ptlinha2)) (- (car ptlinha1) (car ptlinha2))))	

	;pegar o vetor de referencia do poste, ele � o ponto do indentificador do poste em relacao ao ponto de incercao do poste
	(setq incpt (cdr(assoc 10 (entget (entnext postenome)))))
	(setq poste (entget postenome))
	(setq incpt (diferencaPontos incpt (cdr (assoc 10 poste))))
				
	(setq vetor incpt)
	
	(if(= (car ptlinha1) (car ptlinha2))
		(progn ;se a linha for vertical----------------------------------------------------------------------
			(setq grr (append (list 5) (list ptlinha1))); so pra terr certeza que vai entrar no while
			(while (or (= 5 (car grr)) (= 2 (car grr))) ;; enquanto movimenta o cursor ou tecla o teclado
				(if (= 5 (car grr))
					(progn ; se for movimento do mose
						(setq ponto (cadr grr)); p�ga a coordenada do mouse
						(if (> (car ponto) (car ptlinha1)); pra viar o bloco pra cima da linha
							(progn (setq vetor (list (* -1 (car incpt)) (* -1 (cadr incpt)) 0)))
							(progn (setq vetor incpt))
						)
						(setq ponto (list (car ptlinha1) (cadr ponto) 0))							
						(redraw)
						(movetudo postenome (somaPontos ponto vetor))
					
					)
					(progn ; se for um comando do teclado
						(if (= 32 (cadr grr));se a pessoa apertar espa�o
							(progn ; casso queira inserir o poste na posicao TOPO ou NORMAL
								(girarTudo postenome (+ (cdr (assoc 50 (entget postenome))) (/ numpi 2))); girar o poste um 90 graus
							)
						)
					)
				)
			(setq grr (grread t 13 0)); ler a nova coordenada do mouse
			)
		)
		(progn
			(if(= (cadr ptlinha1) (cadr ptlinha2))
				(progn ;se a linha for horizontal-------------------------------------------------------------------
					(setq grr (append (list 5) (list ptlinha1))); so pra terr certeza que vai entrar no while
					(while (or (= 5 (car grr)) (= 2 (car grr))) ;; enquanto movimenta o cursor ou tecla o teclado
						(if (= 5 (car grr))
							(progn ; se for movimento do mose
								(setq ponto (cadr grr)); p�ga a coordenada do mouse
								(if (> (cadr ponto) (cadr ptlinha1)); pra viar o bloco pra cima da linha
									(progn (setq vetor (list (* -1 (car incpt)) (* -1 (cadr incpt)) 0)))
									(progn (setq vetor incpt))
								)
								(setq ponto (list (car ponto) (cadr ptlinha1) 0))							
								(redraw)
								(movetudo postenome (somaPontos ponto vetor))
							
							)
							(progn ; se for um comando do teclado
								(if (= 32 (cadr grr));se a pessoa apertar espa�o
									(progn ; casso queira inserir o poste na posicao TOPO ou NORMAL
										(girarTudo postenome (+ (cdr (assoc 50 (entget postenome))) (/ numpi 2))); girar o poste um 90 graus
									)
								)
							)
						)
					(setq grr (grread t 13 0)); ler a nova coordenada do mouse
					)
				)
				(progn ; se nao � vertical nem horizontal--------------------------------------------------------------------
					;pegar os coeficientes a e b da equacao daquela linha
					(setq a (/ (- (cadr ptlinha1) (cadr ptlinha2))  (- (car ptlinha1) (car ptlinha2)) ))
					(setq b (-(cadr ptlinha1) (*(car ptlinha1) a)) )
					
					
					(setq grr (append (list 5) (list ptlinha1))); so pra terr certeza que vai entrar no while
					(while (or (= 5 (car grr)) (= 2 (car grr))) ;; enquanto movimenta o cursor ou tecla o teclado
						(if (= 5 (car grr))
							(progn ; se for movimento do mose
								(setq ponto (cadr grr)); �ga a coordenada do mouse

								(if (< (abs a) 1)
									(progn
										(if (> (cadr ponto) (valory a b (car ponto))); pra viar o bloco pra cima da linha
											(progn (setq vetor (list (* -1 (car incpt)) (* -1 (cadr incpt)) 0)))
											(progn (setq vetor incpt))
										)
										(setq ponto (list (car ponto) (valory a b (car ponto)) 0))
									)
									(progn
										(if (> (car ponto) (/(-(cadr ponto) b) a)); pra viar o bloco pro lado da linha
											(progn (setq vetor (list (* -1 (car incpt)) (* -1 (cadr incpt)) 0)))
											(progn (setq vetor incpt))
										)
										(setq ponto (list (/(-(cadr ponto) b) a) (cadr ponto) 0))
									)
								)
								(redraw)
								(movetudo postenome (somaPontos ponto vetor))
							
							)
							(progn ; se for um comando do teclado
								(if (= 32 (cadr grr));se a pessoa apertar espa�o
									(progn ; casso queira inserir o poste na posicao TOPO ou NORMAL
										(girarTudo postenome (+ (cdr (assoc 50 (entget postenome))) (/ numpi 2))); girar o poste um 90 graus
									)
								)
							)
						)
					(setq grr (grread t 13 0)); ler a nova coordenada do mouse
					)
				)
			)
		)
	)	
)
;==================================================================================================================================================================
;=================================================================================
(defun inserePoste (EI adesenhar / ID Tipodeposte layeratual saida entidade ponto grr)
	(setq ponto '(0 0 0))
	(setq Tipodeposte (cadr(assoc 98 adesenhar)))
	(setq ID (cadr(assoc 97 adesenhar)))
	(setq layeratual (getvar "clayer"))

	(if (= EI 0)
		(progn ; a instalar
			(setlayer g_layerPostes)
			(if (= Tipodeposte "dt")
				(command "_insert" g_BposteDTExistente ponto "" "" "" ID)
				(command "_insert" g_BposteCCExistente ponto "" "" "" ID)
			)			
		)
		(progn ; existente
			(setlayer g_layerPontosInst )			
			(if (= Tipodeposte "dt")
				(command "_insert" g_BposteDTInstalar ponto "" "" "" ID)
				(command "_insert" g_BposteCCInstalar ponto "" "" "" ID)
			)			
		)
	)
	(setvar "clayer" layeratual)
	(setq saida (entlast))
	(setq entidade (entget saida))
	
	(initget "Livre Alinhado")
	(setq resposta (cond ( (getkword "\nEnter p/ posicionamento Livre... [Livre/Alinhado] <Livre>: ") ) ( "Livre" )))
	(if (= resposta "Alinhado")
		(progn
			(posicionaDTalinhado saida (car(entsel "\nSelecione uma linha: ")))
		)
		(progn
			(posicionaDTlivre saida)
		)
	)
	;(editarposte saida Tipodeposte)
	saida
)
	;===============================================================================
	(defun posicionaDTlivre(postenome / numpi grr pontoref modulo posicao ponto vetores pontoref)
		(setq numpi 3.14159265359)		
		(setq grr (list 5'(0 0 0)))
		
		(setq modulo (distance (cdr (assoc 10 (entget postenome))) (cdr (assoc 10 (entget (entnext postenome))))))
		(setq vetores (list 
						'(0 0 0)
						(polarParaCart (list modulo 0 0)) 
						(polarParaCart (list modulo (* numpi 0.5) 0))
						(polarParaCart (list modulo (* numpi 1) 0))  
						(polarParaCart (list modulo (* numpi 1.5) 0)) 
						)
					)
		(setq posicao 0)
		
		;fun�ao do lee mac, para pegar o osnap
		;(setq osf (LM:grsnap:snapfunction) ;; Define optimised Object Snap function
		;  osm (getvar 'osmode)         ;; Retrieve active Object Snap modes
		;)
		
		(while (or (= 5 (car grr)) (= 2 (car grr)))
			(if (= 5 (car grr))
				(progn	
					(redraw)
					(setq ponto (cadr grr))
					(setq ponto (somaPontos (nth posicao vetores) ponto))					
					;(osf (cadr grr) osm)
					(movetudo postenome ponto)
				)
				(progn
					(if (= 32 (cadr grr));se a pessoa apertar espa�o
						(progn
							(girarTudo postenome (+ (cdr (assoc 50 (entget postenome))) (/ numpi 2)))
						)
						(progn
							(if (= 97(cadr grr))
								(progn
									(setq posicao (+ posicao 1))
									(if (> posicao 4)
										(setq posicao 0)
									)
								)
							)
						)
					)
				)
			)
			(setq grr (grread t 13 0))			
		)
		
		(setq pontoref (diferencaPontos (cdr(assoc 10 (entget postenome))) (nth posicao vetores) ))
		(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
			(redraw)
			;(osf (cadr grr) osm)
			(setq ponto (cadr grr))
			(setq ponto (diferencaPontos pontoref ponto))
			(girarTudoPontobase postenome (+ (atan (cadr ponto) (car ponto)) (* numpi 0.5)) pontoref) 
		)

	)
	; (setq osf (LM:grsnap:snapfunction) ;; Define optimised Object Snap function
		  ; osm (getvar 'osmode)         ;; Retrieve active Object Snap modes
	; )
	; (while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
		; (redraw)             ;; Refresh the display
		; (osf (cadr grr) osm) ;; Calculate & display any available snap points
	; )
	;==================================================================================
	;================================================================================
(defun editarposte (entnome tipo / resposta)
(while (or (initget "Continuar Mover Alinhar") (/= "Continuar" (setq resposta (cond ( (getkword "\nEnter p/ continuar... [Continuar/Mover/Alinhar] <Continuar>: ") ) ( "Continuar" )))))
	(if  (= "Alinhar" resposta)
		(progn 
			(if (= tipo "dt")
				(progn
					(alinhaDT entnome)
				)
				(progn
					(alinhaCC entnome)
				)
			)			
		)
		(progn
			(princ "\nja alinhado.")
		)
	)
	
	(if  (= "Mover" resposta)
		(progn 
			(movebloco entnome)
		)
		(progn
			(princ "\nja no lugar correto.")
		)
	)
	)
	(princ)
)
;======================================================================================
(defun alinhaDT (bloco / osnapvar p1 p2 p3 p4 refponto linharef)

	(setq linharef (entget(car(nentsel "\nSelecione uma linha do poste: "))))	
	(setq p1 (cdr(assoc 10 linharef))) ;em relacao ao bloco
	(setq p2 (cdr(assoc 11 linharef))) ;em relacao ao bloco
	(setq refponto (cdr(assoc 10 (entget bloco)))); ponto de insercao do bloco
	(setq p1 (somaPontos p1 refponto))
	(setq p2 (somaPontos p2 refponto))
	
	
	(setq p3 (getpoint p1 "\nselect point pt3: ")) 
	(setq p4 (getpoint p2 "\nselect point pt4: ")) 
	(setq osnapvar (getvar "osmode"))
	(setvar "osmode" 0)
	(command "align" bloco "" p1 p3 p2 p4 "" "N")
	(setvar "osmode" osnapvar)
)
;========================================================================================
(defun alinhaCC (bloco / numpi)
	(setq numpi 3.14159265359)
	(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
		(setq ponto (cadr grr))
		(setq pontoref (cdr (assoc 10 (entget bloco))))
		(setq ponto (diferencaPontos pontoref ponto))
		(redraw)
		(girarBloco bloco (atan (cadr ponto) (car ponto)) ) 
	)
)
;========================================================================================


