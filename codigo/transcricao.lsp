;Trancrissao do arquivo que realiza o calculo de latlong para XY
;Este � uma copia descarada de um arquivo que peguei na internet


(setq sm_a 6378137.0)
(setq sm_b 6356752.314)
(setq sm_EccSquared 0.00669437999013)

(setq UTMScaleFactor 0.9996)

(defun DegToRad (deg)
	(* pi (/ (float deg) 180))
)

(defun RadToDeg (Rad)
	(* 180 (/ (float Rad) pi))
)

(defun ArcLengthOfMeridian (phi)
	(setq n (/ (- sm_a sm_b) (+ sm_a sm_b)))
	(setq alpha (* (/ (+ sm_a sm_b) 2) (+ 1 (/(expt n 2) 4) (/ (expt n 4)64))))
	(setq beta (+ (/(*)2)))
	(setq gamma (* (/ (+ sm_a sm_b) 2) (+ 1 (/(expt n 2) 4) (/ (expt n 4)64))))
	(setq delta (* (/ (+ sm_a sm_b) 2) (+ 1 (/(expt n 2) 4) (/ (expt n 4)64))))
	(setq epsilon (* (/ (+ sm_a sm_b) 2) (+ 1 (/(expt n 2) 4) (/ (expt n 4)64))))
)