dclPrincipal : dialog {
label = "Alocar Pontos";
 : column {
	  : boxed_column {
		label = "Carregar levantamento" ;
			: boxed_row {
				label = "Postes";
				: edit_box {
					key = "enderecoPostes";
					edit_width = 30;
					fixed_width = true ;
					value = "C:\\Users\\81603074\\Desktop\\solucao-cad\\entradaDados\\levantamento.txt";
					alignment = left;				
				}
				 : button {
					key = "buscarPostes";
					label =  " ... " ;
					is_default = true;
					alignment = right;	
                }
			}
			: boxed_row {
				label = "Cabos";
				: edit_box {
					key = "enderecoCabos";
					edit_width = 30;
					fixed_width = true ;
					value = "C:\\Users\\81603074\\Desktop\\solucao-cad\\entradaDados\\levantamentoCabos.txt";
					alignment = left;				
				}
				 : button {
					key = "buscarCabos";
					label =  " ... " ;
					is_default = true;
					alignment = right;	
                }
			}
			: row {				
				 : button {
					key = "abrir";
					label =  "Abrir" ;
					is_default = false;
					alignment = right;		
                }
				
			}
			: text {
				key = "textconfimacao";
                value = ""; 
			}
		}
		: row {
		 : button {
                key = "ok";
                label = " Ok ";
                is_default = true;
				edit_width = 10;
				alignment = right;
              }
		   : button {
			 key = "cancel";
			 label =  Cancel ;
			 is_default = false;
			 edit_width = 10;
			 alignment = right;
		 }
	}
 }
}