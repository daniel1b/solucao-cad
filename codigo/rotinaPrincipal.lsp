;*******************************************************************************************************************************************************************
;Para poder separar os codigos em varios arquivos criei essa funcao que o carregam
(defun CarregarSubProgramas()
	(load "abrirExportar")
	(load "acessoriosPostes")
;	(load "acessoriosCabos")
	(load "cabos")
	(load "calculoMatricial")
	(load "calculoVetorial")
	(load "calculoEsforcos")
	(load "operacoesBlocos")
	(load "postes")
	(load "tabelas")
	(openconf)
	(princ)
)
(CarregarSubProgramas)
;*******************************************************************************************************************************************************************
;Funcao principal, � nela que o programa comeca
(defun inicializarAlocacaoPontos()
	(CarregarSubProgramas);carrega os demais arquivos necessarios
	(if(not(setq dcl_id (load_dialog "dclPrincipal.dcl"))); procura pelo qrquivo que contem a parte grafica do programa
		(progn ; caso nao encontre, avisar o usuario e sair do programa
		  (alert "Problema ao carregar dlcPrincipal.dcl")
		  (exit)
		);fim progn
		(progn ;se encontrar, buscar nele a definicao para rotinaPrinicpal
			(if (not (new_dialog "dclPrincipal" dcl_id))
				(progn ; se nao encontrar a definicao, avisar o usuario e sair
					(alert "Problema ao carregar a definicao principal do dcl")
					(exit)
				);fim progn
				(progn ;se carregar o arquivo e a definicao, setar as funcoes dos botoes
				
					;se clicar em buscarPoste mudar o campo enderecoPoste para o valor retornado pelo getfiled
					(action_tile "buscarPostes" "(set_tile \"enderecoPostes\" (getfiled \"Buscar Levantamento de Postes:\" \"C:/\" \"txt\" 0))") 
					;se clicar em buscarCabos mudar o campo enderecoCabos para o valor retornado pelo getfiled
					(action_tile "buscarCabos" "(set_tile \"enderecoCabos\" (getfiled \"Buscar Levantamento de Cabos:\" \"C:/\" \"txt\" 0))")
					;se clicar em abrir executar a funcao 'botaoabrir', ela sera definida logo abaixo
					(action_tile "abrir" "(botaoabrir)") 
					;se clicar em cancel, encerra o DLC com codigo 1
					(action_tile "cancel" "(done_dialog 1)")
					;se clicar em ok, encerra o DLC com o codigo 2
					(action_tile "ok" "(done_dialog 2)")
					;desativar o botao ok, o usuario so poder� clicar nele apos carregar o levantamento
					(mode_tile "ok" 1)
					
					(defun botaoabrir();definindo a funcao abrir
						; tentar setar as variaveis objPostes e objCabos, caso nao consiga os valores serao nil, portanto nao entrar� no if
						(if (and (setq objPostes (carregaLevantamento (get_tile "enderecoPostes")) ) (setq objCabos (carregaLevantamento (setq g_enderecoCabo(get_tile "enderecoCabos")))))
							(progn ; caso consiga carregar os dois levantamento
								(set_tile "textconfimacao" "Levantamento carregado com sucesso!"); avisar que deu certo
								(mode_tile "ok" 0);desbloquar o botao OK
							);fim progn
						);fim if
					); fim func
					
					;apos todo dialogo estar definido, inicializa-lo
					(setq ddiag(start_dialog)) ; salva na ddiag 2 ou 1, se for encerrado por ok ou cancel, respectivamente
					(unload_dialog dcl_id); apaga o DLC da memoria
					(if (= ddiag 1); se tiver saido com cancel
						(progn
							(princ "\nCancelado");avisar 
							(exit);sair
						)
					)
					(if (= ddiag 2); se tiver saido com ok
						(progn			
							(colocaPontos objPostes) ; comecar a colocar os postes
							(colocaCabos objCabos);comecar a colocar os cabos
						);fim progn
					);fim if				  
				);fim progn
			);fim if
		);fim progn
	); fim if
	(princ); retorna vazio
);fim funcao inicializarAlocacaoPontos
;*******************************************************************************************************************************************************************
; essa funcao defne a ordem em que vai ser colocado os elementos de um ponto
(defun colocaPontos(levantamento / resposta lastLuminaria lasttabela contador adesenhar lastposte lastID texto)	
	
	;primeira coisa a ser feita � descobrir quais postes ja foram alocados		
	(setq listaIDcolocados (list 0)); iniciar uma lista, aqui serao colocados o num dos que ja foram alocados
	(if (setq postesColocados (ssget "x" (list (cons 2  "CONTROLE_PONTO"))));selecionar todos os indetificadores
		(progn ; caso a selecao nao esteje vazia
			(setq quantidade (sslength postesColocados)); seta quantidade com o tamanho da selecao
			(setq contadorSS 0); crio um contador para a selecao, e coloco ele no comeco
			(while (< contadorSS quantidade); rodar enquanto o contador for menor que o tamanho da selecao
				(setq entityNome (ssname postesColocados contadorSS)); pegar o nome da "contador"iezima entidade da selecao
				(setq subentityNome entityNome); pegar o codigo dxf da entidade
				(while (/= (cdr( assoc 0 (entget(setq subentityNome (entnext subentityNome))))) "SEQEND"); procurar pelo atributo associado a entidade
					(if (and(= (cdr(assoc 0 (entget subentityNome))) "ATTRIB")(= (cdr(assoc 2 (entget subentityNome))) "IDlevantamento")); se uma das subentidades � um atributo, adicionar a lista
						(progn
							(setq texto (cdr (assoc 1 (entget subentityNome))))
							(setq  listaIDcolocados (append listaIDcolocados (list (atoi (car(explode 44 texto))))))
						);fim prog
					); fim if
				); fim while
			(setq contadorSS (+ contadorSS 1)) ; somar um no contador para ir para a proxima entidade
			); fim while
		); fim progn
	); fim if
	;ja se sabe quais postes ja foram colocados, estao armazenados em listaIDcolocados
	;(princ listaIDcolocados)
	;------------------------------------------------------
	;comecar a colocar os postes do levantamento
	(setq contador 1); comecar pelo poste de numero 1
	(while (setq adesenhar (cdr (assoc contador levantamento))); enquanto ha postes no levantamento		
		(princ "\nPoste")
		(princ contador)
		(if (not (PertenceLista contador listaIDcolocados));verificar se ele ja esta no desenho
			(progn ;caso nao esteja comecar a colocar
				;(setq adesenhar (corrigirPonto adesenhar)); essa funcao verifica se o usuario quer modificar o ponto
				;(atualizaID adesenhar)
				(setq adesenhar (subst (cons 97 (list(itoa(+ (getultimoID) 1))))(assoc 97 adesenhar) adesenhar))
				(setq lastposte (inserePoste 0 adesenhar)); colocar o poste
				(setq lastluminaria (insereLuminaria 0 adesenhar lastposte)); colocar a luminaria
				(setq lastchave (inserechave 0 adesenhar lastposte)); colocar a chavefusivel
				(setq lasttrafo(inseretrafo 0 adesenhar lastchave lastposte)); colocar o trafo
				(setq lastpararaio(inserepararaio 0 adesenhar lasttrafo lastchave lastposte)); colocar o pararaio
				(setq lastaterramento(insereaterramento 0 adesenhar lastpararaio lasttrafo lastchave lastposte));colocar o aterramento
				(setq lasttabelaposte(insereTabelaPoste 0 adesenhar ));colocar a tabela do poste
				(setq lasttabelaposto(insereTabelaPosto 0 adesenhar ));colocar a tabela do posto
				(setq lastID (insereID adesenhar));colocar o indentificador do ponto
				(setq controle (inserecontrole adesenhar lastposte contador))
				(initget "Continuar Parar");definir possiveis respostas
				;perguntar se o usuario deseja colocar o proximo
				(setq resposta (cond ( (getkword "\nEnter p/ continuar... [Continuar/Parar] <Continuar>: ") ) ( "Continuar" )))
				(if (= resposta "Parar")
					(progn (exit))
				)
				(princ "\nPoste")
				(princ contador)				
			);fim progn
		);fim if
		(princ "- Presente\n")
		(setq contador (+ contador 1)); somar um no contador para ir para a proxima variavel
	)
	(alert "\nTodos os postes foram colocados");avisar que todos os postes foram colocados
	(princ)
)

; ;=============================================================================================================================
; (defun atualizaID (apartir / contador )
	; (setq oldID apartir)
	; (setq newID (1+ (getultimoID)))
	; (setq diferenca (- newID oldID))
	; (setq contador oldID)
	; (setq oldObjPostes objPostes)	
		
	; (while (setq elemento (cdr (assoc contador oldObjPostes)))
		; (setq elemento (subst (cons 97 (list(itoa (+ diferenca (atoi (cadr (assoc 97 elemento))))))) (assoc 97 elemento) elemento))
		; ;(princ elemento)
		; (setq objPostes (subst (cons (+ contador diferenca) elemento ) (assoc contador objPostes) objPostes))
		; (setq contador (1+ contador))
	; )
	; objPostes
; )

;=============================================================================================================================
; (defun ultimoLevantamento( / contador limite saida )
	; (setq limite (length objPostes))
	; (setq contador 0)
	; (setq saida (caar objPostes))
	; (while (< contador limite)
		; (if (< saida (car (nth contador objPostes)))
			; (setq saida (car (nth contador objPostes)))
		; )
		; (setq contador (1+ contador))
	; )
	; saida
;)
;=============================================================================================================================
(defun ContinuarAlocacaoPontos()
	(if objPostes
		(colocaPontos objPostes)
		(alert "Voce deve carregar o levantamento Primeiro.")
	)
	(if g_enderecoCabo
		(progn
			(setq objCabos (carregaLevantamento g_enderecoCabo))
			(colocaCabos (princ(definecaminholevantamento)))
		)
	)
(princ)
)
;=============================================================================================================================
(defun selPnt( / saida)
	(setq num (getstring "\nDigite o numero do ponto: "))
	(if num
		(progn
			(setq saida (selecionaPnt (atoi num)))
		)
	)

saida
)
;=============================================================================================================================
(defun getultimoID( / saida listaIDcolocados postesColocados quantidade contadorSS entityNome subentityNome)
	(setq saida 0)
	(setq listaIDcolocados (list 0)); iniciar uma lista, aqui serao colocados o num dos que ja foram alocados
	(if (setq postesColocados (ssget "x" (list (cons 2  "ID_PONTO"))));selecionar todos os indetificadores
		(progn ; caso a selecao nao esteje vazia
			(setq quantidade (sslength postesColocados)); seta quantidade com o tamanho da selecao
			(setq contadorSS 0); crio um contador para a selecao, e coloco ele no comeco
			(while (< contadorSS quantidade); rodar enquanto o contador for menor que o tamanho da selecao
				(setq entityNome (ssname postesColocados contadorSS)); pegar o nome da "contador"iezima entidade da selecao
				(setq subentityNome entityNome); pegar o codigo dxf da entidade
				(while (/= (cdr( assoc 0 (entget(setq subentityNome (entnext subentityNome))))) "SEQEND"); procurar pelo atributo associado a entidade
					(if (= (cdr(assoc 0 (entget subentityNome))) "ATTRIB"); se uma das subentidades � um atributo, adicionar a lista
						(progn
							(setq  listaIDcolocados (append listaIDcolocados (list (atoi (cdr (assoc 1 (entget subentityNome)))))))
						);fim prog
					); fim if
				); fim while
			(setq contadorSS (+ contadorSS 1)) ; somar um no contador para ir para a proxima entidade
			); fim while
		); fim progn
	)
	
	(foreach a listaIDcolocados (if (> a saida) (progn (setq saida a))))
	saida
)
;==================================================================================================================================================================
(defun botaoAddposteExis ( / adesenhar id)
	(CarregarSubProgramas)
	
	(setq id (+ (getultimoID) 1))
	(setq id (itoa id))
	
	
	
	;----- criar um objeto para desenhar
	(setq adesenhar (list
			(cons 97 (list id))
			(cons 98 (list "cc"))
			(cons 99 (list "11/300"))
			;(cons 100 (list "A2-2A1"))
			;(cons 101 (list "N2-N1"))
			(cons 102 (list "ilegivel"))
			(cons 103 (list "7"))
			;(cons 104 (list "PARARAIO"))
			;(cons 105 (list "CHAVEFUSIVEL"))
			;(cons 106 (list "75KVA" "GN-11" "346268"))
			;(cons 107 (list "ATERRAMENTO"))
			
		)
	)
	;------
	;(princ adesenhar)
	(setq adesenhar (corrigirPontoExistente adesenhar))
	(if adesenhar
		(progn
			(if (posteentnome (atoi (cadr (assoc 97 adesenhar))))
				(liberaID (atoi (cadr (assoc 97 adesenhar))))		
			)
			(setq lastposte (inserePoste 0 adesenhar)); colocar o poste
			(setq lastluminaria (insereLuminaria 0 adesenhar lastposte)); colocar a luminaria
			(setq lastchave (inserechave 0 adesenhar lastposte)); colocar a chavefusivel
			(setq lasttrafo(inseretrafo 0 adesenhar lastchave lastposte)); colocar o trafo
			(setq lastpararaio(inserepararaio 0 adesenhar lasttrafo lastchave lastposte)); colocar o pararaio
			(setq lastaterramento(insereaterramento 0 adesenhar lastpararaio lasttrafo lastchave lastposte));colocar o aterramento
			(setq lasttabelaposte(insereTabelaPoste 0 adesenhar ));colocar a tabela do poste
			(setq lasttabelaposto(insereTabelaPosto 0 adesenhar ));colocar a tabela do posto
			(setq lastID (insereID adesenhar));colocar o indentificador do ponto
		)
	)
	(princ)
)
;=============================================================================================================================
(defun botaoAddposteInst ( / adesenhar id)
	(CarregarSubProgramas)
	
	(setq id (+ (getultimoID) 1))
	(setq id (itoa id))
	
	;----- criar um objeto para desenhar
	(setq adesenhar (list
			(cons 97 (list id))
			(cons 98 (list "cc"))
			(cons 99 (list "11/300"))
			;(cons 100 (list "A2-2A1"))
			;(cons 101 (list "N2-N1"))
			(cons 103 (list "7"))
			;(cons 104 (list "PARARAIO"))
			;(cons 105 (list "CHAVEFUSIVEL"))
			;(cons 106 (list "75KVA" "GN-11" "346268"))
			;(cons 107 (list "ATERRAMENTO"))
			
		)
	)
	;------
	;(princ adesenhar)
	(setq adesenhar (corrigirPontoInstalar adesenhar))
	(if adesenhar
		(progn
			(if (posteentnome (atoi (cadr (assoc 97 adesenhar))))
				(liberaID (atoi (cadr (assoc 97 adesenhar))))		
			)
			(setq lastposte (inserePoste 1 adesenhar)); colocar o poste
			(setq lastluminaria (insereLuminaria 1 adesenhar lastposte)); colocar a luminaria
			(setq lastchave (inserechave 1 adesenhar lastposte)); colocar a chavefusivel
			(setq lasttrafo(inseretrafo 1 adesenhar lastchave lastposte)); colocar o trafo
			(setq lastpararaio(inserepararaio 1 adesenhar lasttrafo lastchave lastposte)); colocar o pararaio
			(setq lastaterramento(insereaterramento 1 adesenhar lastpararaio lasttrafo lastchave lastposte));colocar o aterramento
			;(setq lasttabelaposte(insereTabelaPoste 1 adesenhar ));colocar a tabela do poste
			;(setq lasttabelaposto(insereTabelaPosto 1 adesenhar ));colocar a tabela do posto
			(setq lastID (insereID adesenhar));colocar o indentificador do ponto
		)
	)
	(princ)
)
;botaoAddposteInst

;*******************************************************************************************************************************************************************
(princ)