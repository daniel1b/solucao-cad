(defun selecionaCabos(postenome / entidade  pntIncersao pntIncersaoID vetor lista limite contador saida polilinhas)

	(setq pntIncersao (cdr(assoc 10 (entget postenome))))
	(setq pntIncersaoID (cdr(assoc 10 (entget (entnext postenome)))))
	
	(setq vetor (diferencaPontos pntIncersaoID pntIncersao))
	(setq vetor (cartParaPolar vetor))
	(setq vetor (polarParaCart (list (* 2 (car vetor)) (cadr vetor))))
	
	(setq lista (list))
	(setq limite 8)
	(setq contador 0)
	(while (< contador limite)
		(setq lista (append lista (list (somaPontos pntIncersao (rotVetor vetor (* (+ 1 contador) (/ (* 2 3.14159) limite)))))))
		(setq contador (1+ contador))
	)
	;(princ lista)
	(setq saida (ssget "_CP" lista))
	(setq limite (sslength saida))
	(setq contador 0)
	(setq polilinhas (ssadd))
	(while (< contador limite)
		(setq entidade (ssname saida contador))
		(if (and entidade (= "LWPOLYLINE" (cdr ( assoc 0 (entget entidade)))))
			(setq polilinhas (ssadd entidade polilinhas))
		)
		(setq contador (+ 1 contador))
	)
	polilinhas
)
;==============================================================================================================
(defun unitario(vetor / modulo saida)
	(setq modulo (sqrt (+ (*(car vetor)(car vetor)) (*(cadr vetor)(cadr vetor)))))
	(setq saida (list (/ (car vetor) modulo) (/ (cadr vetor) modulo) 0))
	saida
)
(defun modDir(mod vetor / modulo saida)
	(setq modulo (sqrt (+ (*(car vetor)(car vetor)) (*(cadr vetor)(cadr vetor)))))
	(setq saida (list (* mod (/ (car vetor) modulo)) (* mod (/ (cadr vetor) modulo)) 0))
	saida
)
;==============================================================================================================
(defun getVetores (postenome / contador listaVetores entidade vertices contadorPontos limitePontos pntIncersao pntIncersaoID vetor modulo polilinhas limite)
	(setq pntIncersao (cdr(assoc 10 (entget postenome))))
	(setq pntIncersaoID (cdr(assoc 10 (entget (entnext postenome)))))
	
	(setq vetor (diferencaPontos pntIncersaoID pntIncersao))
	(setq vetor (cartParaPolar vetor))
	(setq modulo (* 2 (car vetor)))
	
	(setq polilinhas (selecionaCabos postenome))
	(setq limite (sslength polilinhas))
	(setq contador 0)
	(setq listaVetores (list))
	(while (< contador limite)
		(setq entidade (ssname polilinhas contador))
		(setq modTcabo (moduloTracao (nth 1 (legAssocCabo entidade)) (cdr (assoc 8 (entget entidade))) 10))
		(setq vertices (getpontos entidade))
		(setq contadorPontos 0)
		(setq limitePontos (length vertices))
		(while (< contadorPontos limitePontos)
			(setq pnt (nth contadorPontos vertices))
			(if (< (distance pnt pntIncersao) modulo)
				(progn
					(if (and (< contadorPontos (- limitePontos 1)) (> contadorPontos 0))
						(progn
							(if (>(car(cartParaPolar(diferencaPontos (nth (+ contadorPontos 1) vertices) pnt ))) (* 2 modulo))
								(setq listaVetores (append listaVetores (list (modDir modTcabo (diferencaPontos (nth (+ contadorPontos 1) vertices) pnt )))))
							)
							(if (>(car(cartParaPolar(diferencaPontos (nth (- contadorPontos 1) vertices) pnt ))) (* 2 modulo))
								(setq listaVetores (append listaVetores (list (modDir modTcabo (diferencaPontos (nth (- contadorPontos 1) vertices) pnt )))))
							)
						)
						(progn
							(if (= contadorPontos  (- limitePontos 1))
								(progn
									(if (>(car(cartParaPolar(diferencaPontos (nth (- contadorPontos 1) vertices)pnt))) (* 2 modulo))
										(setq listaVetores (append listaVetores (list (modDir modTcabo (diferencaPontos (nth (- contadorPontos 1) vertices)pnt)))))
									)
								)
								(progn
									(if (= contadorPontos 0)
										(if (>(car(cartParaPolar(diferencaPontos (nth (+ contadorPontos 1) vertices) pnt))) (* 2 modulo))
											(setq listaVetores (append listaVetores (list (modDir modTcabo (diferencaPontos (nth (+ contadorPontos 1) vertices) pnt)))))
										)
									)
								)
							)
						)
					)										
				)
			)			
			(setq contadorPontos (1+ contadorPontos))
		)
		(setq contador (1+ contador))
	)
	listaVetores
)
;==============================================================================================================
(defun moduloTracao (legenda tensao tamanho / H resultante hcabo quantfases tracaoFase contador cabo)
	(setq H (- tamanho (+ 0.8 (* tamanho 0.1))))
	(if (= tensao (nth 0 g_LayerCabosBaixa)) ; ve se o cabo é de baixa ou alta tensao
		(progn ;se for da baixa
			(setq resultante 0)
			(setq hcabo 7.3)
			;para o neutro
			(if (setq cabo (stringEntreD 40 41 legenda));pegar o que esta entre ( )
				(progn
					(setq resultante (+ resultante (* (tracaoPcabo cabo) (/ hcabo H))))
					(setq hcabo (- hcabo 0.2))
				)
			)
			;para as fases
			(setq quantfases (atoi(stringantesde 35 legenda)))
			(setq tracaoFase (tracaoPcabo (cond ((stringEntreD 35 40 legenda)) ((stringEntreD 35 67 legenda)))))
			(setq contador 0)
			(while (< contador quantfases)
				(setq resultante (+ resultante (* tracaoFase (/ (- hcabo (* contador 0.2)) H))))
				(setq contador (1+ contador))
			)
		)
		(progn
			(if(= tensao (nth 0 g_LayerCabosAlta))
				(progn ;se for da alta
					(setq resultante 0)
					;para o neutro
					(if (setq cabo (stringEntreD 40 41 legenda));pegar o que esta entre ( ) se tiver
						(progn
							(if (or (= (stringEntreD 45 44 legenda) " 34")
									  (= (stringEntreD 45 46 legenda) " 34"))
								(progn (setq hcabo (- H 1))) ; se for rede de 34kv
								(progn (setq hcabo (- H 0.8))); se for rede de 13.8kv
							)
							(setq resultante (+ resultante (* (tracaoPcabo cabo) (/ hcabo H))))
						)
					)
					;para as fases
					(setq quantfases (atoi(stringantesde 35 legenda)))
					(setq tracaoFase (tracaoPcabo (cond ((stringEntreD 35 40 legenda)) ((stringEntreD 35 67 legenda)))))
					(setq resultante (+ resultante (* tracaoFase quantfases)))
				)
			)
		)
	)
	resultante
)
;==============================================================================================================
(defun tracaoPcabo(texto / saida)
	(if (= texto "4")
		(setq saida (nth 0 g_4AWC/CA))
	)
	(if (= texto "2")
		(setq saida (nth 0 g_2AWC/CA))
	)
	(if (= texto "1/0")
		(setq saida (nth 0 g_1/0AWC/CA))
	)
	(if (= texto "2/0")
		(setq saida (nth 0 g_2/0AWC/CA ))
	)
	(if (= texto "4/0")
		(setq saida (nth 0 g_4/0AWC/CA))
	)
	(if (or(= texto "336.4") (= texto "336,4") (= texto "336"))
		(setq saida (nth 0 g_336.4CA))
	)
	(if (= texto "50mmXLPE")
		(setq saida (nth 0 g_50mmXLPE))
	)
	(if (= texto "95mmXLPE")
		(setq saida (nth 0 g_95mmXLPE))
	)
	(if (= texto "150mmXLPE")
		(setq saida (nth 0 g_150mmXLPE))
	)
	saida
)
;==================================================================================================================
(defun legAssocCabo (polilinha)
	(setq saida (list))
	;pra diminuir as contar pegar apenasas legendas relacionadas ao mesmo layer do cabo
	(setq LegCabos (ssget "_X" (list (cons 0 "INSERT") (cons 2 g_textoCabo) (cons 8 (cdr (assoc 8 (entget polilinha)))))))
	(setq contadordeLegCabos 0)
	
		(while (and (< contadordeLegCabos (sslength LegCabos))
			(/= 0 (setq distanciaCabolinha (round (distanciaPolinhaPonto polilinha (cdr (assoc 10 (entget(setq entLeg (ssname LegCabos contadordeLegCabos)))))) 0.01))))
			(setq contadordeLegCabos (1+ contadordeLegCabos))
		)
		
		(if (= distanciaCabolinha 0)
			(progn
				(setq subentLeg (entnext entLeg))
				(while (= "ATTRIB" (cdr (assoc 0 (entget subentLeg))))
					(if (=  (cdr (assoc 2 (entget subentLeg))) "Legenda")
						(progn
							(setq texto (cdr (assoc 1 (entget subentLeg))))
						)
						(progn
							(if (=  (cdr (assoc 2 (entget subentLeg))) "IDcabo")
								(progn
									(setq ID (cdr (assoc 1 (entget subentLeg))))
								)
							)
						)
					)
					(setq subentLeg (entnext subentLeg))		
				)
			)
		)
	(setq saida (list ID texto))
)
;==============================================================================================================
(defun CalcResultante(postenome)
	(setq vetores (getVetores postenome))
	(setq resultante (list 0 0 0))
	
	(setq maiorModulo (car(cartParaPolar (nth 0 vetores))))
	
	(setq limite (length  vetores))
	(setq contador 0)
	(while (< contador limite)
		(if (> (car(cartParaPolar (nth contador vetores))) maiorModulo)
			(setq maiorModulo (car(cartParaPolar (nth contador vetores))))
		)
		(setq resultante (somaPontos resultante (nth contador vetores)))
		(setq contador (1+ contador))
	)
	(setq moduloResultante (car(cartParaPolar resultante)))
	
	(if (> moduloResultante maiorModulo)
		(setq maiorModulo moduloResultante)
	)	
	
	(setq contador 0)
	(while (< contador limite)
		; linha abaixo mostra os vetores pequenos
		;(insertVetor (nth contador vetores) maiorModulo (cdr(assoc 10 (entget postenome))) nil)
		(setq contador (1+ contador))
	)
	(insertVetor resultante maiorModulo (cdr(assoc 10 (entget postenome))) T)	
	(princ )
)
;==============================================================================================================
(defun insertVetor(vetor modulomaximo ponto flag / moduloVetor escala angulo layeratual)
	(setq moduloVetor (car(cartParaPolar vetor)))
	(setq escala (/ moduloVetor modulomaximo))
	(setq angulo (atan (cadr vetor) (car vetor)))
	(setq layeratual (getvar "clayer"))
	(setlayer g_layerVetores)
	(if flag
		(progn(command "_insert" "VETOR-RESULTANTE" ponto "1" "1" 0 (strcat (rtos moduloVetor 2 2) " daN")))
		(progn(command "_insert" "VETOR" ponto "1" "1" 0 (strcat (rtos moduloVetor 2 2) " daN")))
	)
	(escalaBlocoVetor (entlast) escala)
	(girarBlocoVetor (entlast) angulo)
	(setvar "clayer" layeratual)
	(princ)
)
;==============================================================================================================


(princ)
