;====================================================
(defun PertenceLista (valor lista / valorTeste contador saida)
	(setq saida nil)
	(if lista
		(progn
			(setq contador 0)
			(while (setq valorTeste (nth contador lista))
				(if (= valor valorTeste)
					(progn
						(setq saida contador)
					)
				)
				(setq contador (+ contador 1))
			)
		)
	)		
	saida
)
;*******************************************************************************************************************************************************************
; essa funcao verifica se existe o elemento valor em lista
; (defun PertenceLista (valor lista / valorTeste contador saida)
	; (setq saida nil)
	; (setq contador 0)
	; (while (setq valorTeste (nth contador lista))
		; (if (= valor valorTeste)
			; (progn
				; (setq saida T)
			; )
		; )
		; (setq contador (+ contador 1))
	; )	
	; saida
; )
;===============================================================================================================================
;essa funcao retorna a diferenca entre dos pontos c b
(defun diferencaPontos(c b / saida)
	(setq saida (list (- (car c) (car b)) (-(cadr c) (cadr b)) (- (car(cdr(cdr c))) (car(cdr(cdr b))))))
)
;===============================================================================================================================
;essa funcao retorna a soma entre os pontos c b
(defun somaPontos(c b / saida)
	(setq saida (list (+ (car c) (car b)) (+(cadr c) (cadr b)) (+ (car(cdr(cdr c))) (car(cdr(cdr b))))))
)
;==============================================================
(defun settermolinha(m i valor / contadori contadorj linha saida auxiliar Mi Mj); matriz , 1 dim, 2 dim, valor a ser substituido
		;pegar linhas e colunas de M
		(setq Mi 0)
		(while (/= (nth Mi M) nil) (setq Mi (+ Mi 1)))
		;-----------
		(setq saida (list)); lista vazia
		(setq contadori 0)
		(while (< contadori Mi)
			(if  (= contadori i)
				(progn
					(setq auxiliar valor)
				)
				(progn
					(setq auxiliar (nth contadori m))
				)				
			)
		(setq saida (append saida (list auxiliar)))
		(setq contadori (+ contadori 1))
		)		
		saida
	)
;===============================================================================================================================
;esta funcao transforma um ponto de coordenadas cartesianas para coordenadas polares
(defun  cartParaPolar(A / modulo saida angulo numpi)
	(setq numpi 3.14159265359)
	(setq modulo (sqrt (+ (expt (car A) 2.0) (expt (cadr A) 2.0))))
	(setq angulo (atan (cadr A) (car A)))
	;(setq angulo (/ (* 180.0 angulo) numpi)) ; passa o angulo para graus
	(setq saida (list modulo angulo))
saida
)
;===============================================================================================================================
;esta funcao transforma um ponto de coordenadas polares para coordenadas cartesianass
(defun polarParaCart (A / saida numpi x y)
	(setq numpi 3.14159265359)
	(setq x (* (car A) (cos (cadr A))))
	(setq y (* (car A) (sin (cadr A))))
	(setq saida (list x y 0))
	saida
)
;===============================================================================================================================
;essa funcao adiciona ang a um vetor, note que este tem que estar em coordenadas cartesianas
(defun rotVetor ( vetor ang / saida)
	(setq vetor (cartParaPolar vetor))
	(setq vetor (list (car vetor) (+(cadr vetor) ang)))
	(setq vetor (polarParaCart vetor))
	(setq saida vetor)
	saida
)
;===============================================================================================================================
;essa funcao determina se o valor x pertence ou nao ao intervalo a<x<b
;T para sim, nil para nao
(defun pertence(x a b / saida)
	(if (or (and (< x a) (> x b)) (and (> x a) (< x b)))
		(progn 
			(setq saida T)
		)
		(progn 
			(setq saida nil)
		)
	)
	saida 
)
;===============================================================================================================================
;retorna o valor de y = ax + b, funcao de primeiro grau
(defun valory (a b x / saida)
(setq saida (+ b (* a x)))
saida
)
;===============================================================================================================================
;dados os pontos pt1 e pt2 que determinam uma linha, essa funcao retorna a menor distancia entre essa linha e um pt3
;a ideia eh pegar um vetor na deirecao da linha, pegar outro vetor ortogonal a ele
;achar a equacao da linha que passa por pt3 e eh ortogonal a linnha dada
;encontrar pt4 que � a intesecao das duas linhas
;calcular a distncia pt3 e pt4
(defun distanciaLinhaPonto( pt1 pt2 pt3 / linha a12 b12 a34 b34 MATcoef MATindep saida vetor12 vetorOrt12 resultado pt4 )
	(load "calculoMatricial"); para poder achar a intercecao de duas linhas
	;(setq linha (entget linhanome))
	;	(setq pt1 (cdr(assoc 10 linha)))
	;	(setq pt2 (cdr(assoc 11 linha)))
		(if(= (car pt1) (car pt2))
			(progn
				(if (pertence (cadr pt3) (cadr pt1) (cadr pt2))
					(progn
						(setq saida (abs(- (car pt1) (car pt3))))
					)
					(progn
						(setq saida nil)	
					)
				)			
			)
			(progn
				(if(= (cadr pt1) (cadr pt2))
					(progn
						(if (pertence (car pt3) (car pt1) (car pt2))
							(progn
								(setq saida (abs(- (cadr pt1) (cadr pt3))))
							)
							(progn
								(setq saida nil)	
							)
						)
					)
					(progn
						(setq vetor12 (diferencaPontos pt2 pt1))
						(setq vetorOrt12 (list (cadr vetor12) (* -1 (car vetor12)) 0))
						(setq a12 (/ (cadr vetor12) (car vetor12)))
						(setq b12 (- (cadr PT1) (* a12 (car PT1))))
						(setq a34 (/ (cadr vetorOrt12) (car vetorOrt12)))
						(setq b34 (- (cadr pt3) (* a34 (car pt3))))
						(setq MATcoef (list (list a12 -1) (list a34 -1)))
						(setq MATindep (list (list (* -1 b12)) (list (* -1 b34))))
						(setq resultado (resolvesistema MATcoef MATindep))
						(setq pt4 (list (caar resultado) (caadr resultado) 0))
						(if (pertence (car PT4) (car pt1) (car pt2))
							(progn
								(setq saida (DISTANCE PT3 PT4))							
							)
							(progn
								(setq saida nil)	
							)
						)
					)
				)
			)
		)	
		saida
)
;=============================================================================
(defun getpontos(polinha / pontosdalinha saida a contador)
	(setq polinha(entget polinha))
	(setq pontosdalinha(list))
	(setq saida(list))
	(foreach a polinha
		(if(= 10 (car a))
			(setq pontosdalinha (append pontosdalinha (list (list (car (cdr a)) (cadr (cdr a)) 0))))   
		)    
	)
	;Eliminar pontos repetidos
	(setq saida (list (nth 0 pontosdalinha) (nth 1 pontosdalinha)))
	(setq contador 2)
	(while (< contador (length pontosdalinha))	
		(if (comparaPontos (nth (- (length saida) 2) saida) (nth (- (length saida) 1) saida) (nth contador pontosdalinha))
			(progn
				(setq saida (alterarUltimo saida (nth contador pontosdalinha)))
			)
			(progn
				(setq saida (append saida (list (nth contador pontosdalinha))))
			)
		)
	(setq contador (1+ contador))
	)	
	
	saida
)
;==============================================================================
(defun round (num prec)
 (if num
	(progn
		(* prec
			(if (minusp num)
				(fix (- (/ num prec) 0.5))
				(fix (+ (/ num prec) 0.5))
			)
		)
	)
 ) 
)
;==============================================================================
(defun alterarUltimo(m valor / contadori auxiliar Mi); matriz , 1 dim, 2 dim, valor a ser substituido
	;pegar linhas e colunas de M
	(setq Mi 0)
	(while (/= (nth Mi M) nil) (setq Mi (+ Mi 1)))
	(setq Mi (1- Mi))
	;-----------
	(setq auxiliar (list)); lista vazia
	(setq contadori 0)
	(while (< contadori Mi)
		(setq auxiliar (append auxiliar (list (nth contadori m))))
	(setq contadori (+ contadori 1))
	)		
	(setq auxiliar (append auxiliar (list valor)))
	auxiliar
)
;==============================================================================
(defun comparaPontos (a b c / saida vt1 vt2)
	(setq saida nil)
	(if (or (and (= (car a) (car b)) (= (car a) (car c))) (and (= (cadr a) (cadr b)) (= (cadr a) (cadr c))))
		(progn ; se os pontos pertencem a uma mesma linha vertical ou horizontal
			(setq saida T)
		)
		(progn ; se os pontos possuem o mesmo coeficente angular
			(setq vt1 (diferencaPontos b a))
			(setq vt2 (diferencaPontos c a))
			(if (or (= (round (atan (cadr vt1) (car vt1)) 0.001) (round (atan (cadr vt2) (car vt2)) 0.001)))
				(progn
					(setq saida T)
				)
			)
		)
	)
)
;====================================================================================
(defun linhaMaisProxima (pontosdalinha ponto / a contador pontoA pontoB menorDist pontosdalinha distanciaatual  ponto)
		
	(setq pontoA (nth 0 pontosdalinha))
	(setq pontoB (nth 1 pontosdalinha))
	
	(setq contador 0)
	(setq menorDist (distanciaLinhaPonto pontoA pontoB ponto))
	(while (and (not menorDist)(< contador (- (length pontosdalinha) 1)))
		(setq menorDist (distanciaLinhaPonto (nth contador pontosdalinha) (nth (+ contador 1) pontosdalinha) ponto))		
	(setq contador (+ contador 1))
	)
	
	(if menorDist
		(progn
			(setq contador 0)
			(while (< contador (- (length pontosdalinha) 1))
				(setq distanciaatual (distanciaLinhaPonto (nth contador pontosdalinha) (nth (+ contador 1) pontosdalinha) ponto))
				(if distanciaatual
					(progn
						(if (<= distanciaatual menorDist)
							(progn
								(setq menorDist distanciaatual)
								(setq pontoA (nth contador pontosdalinha))
								(setq pontoB (nth (+ contador 1) pontosdalinha))
							)
						)						
					)
				)
			(setq contador (+ contador 1))
			)
		)
	)
	; (getpoint pontoA)
	; (getpoint pontoB)
	(list pontoA pontoB)
)
;===============================================================================================================================