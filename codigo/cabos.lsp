;===================================================================
(defun eliminaPntRepetidoPline (plinha / pnt entidade saida contador elemento )
	(setq entidade (entget plinha))
	(setq saida (list))
	(setq contador 0)
	(while (setq elemento (nth contador entidade))
		(if (=(car elemento)10)
			(progn
				(if pnt
					(progn						
						(if (/= 0 (round (distance (cdr elemento) pnt) 0.01))
							(progn
								(setq saida(append saida (list elemento)))
								(setq saida(append saida (list (nth (+ 1 contador) entidade))))
								(setq saida(append saida (list (nth (+ 2 contador) entidade))))
								(setq saida(append saida (list (nth (+ 3 contador) entidade))))						
							)
						)
						(setq pnt (cdr elemento))
						(setq contador (+ contador 3))
					)
					(progn
						(setq saida(append saida (list elemento)))
						(setq pnt (cdr elemento))
					)
				)				
			)
			(progn
				(setq saida(append saida (list elemento)))
			)
		)
		(setq contador (1+ contador))
	)
	(entmod saida)
	plinha
)
;====================================================================
(defun insereNaPlinha(plinha nBloco layerDesenhar num / vetor ponto angulo layeratual lastEncabecamento poste insPoste modulo listaPontos contador menordistancia)
	(setq poste (entget(posteEntNome num)))
	(setq insPoste (cdr(assoc 10 poste)))
	(setq modulo (distance insPoste (cdr(assoc 10 (entget(entnext (posteEntNome num)))))))
	
	(eliminaPntRepetidoPline plinha)
	(setq listaPontos (getpontos plinha))
	
	(setq contador 0)
	(setq menordistancia (list 0 (distance (nth 0 listaPontos) insPoste)))
	(while (< contador (length listaPontos))
		(if (< (distance (nth contador listaPontos) insPoste) (cadr menordistancia))
			(setq menordistancia (list contador (distance (nth contador listaPontos) insPoste)))
		)
		(setq contador(1+ contador))
	)

	(if (and (< (cadr menordistancia) (* 2 modulo)) (or(= (car menordistancia) 0)(= (car menordistancia) (- (length listaPontos) 1))))
		(progn
			(if (= (car menordistancia) (- (length listaPontos) 1))
				(progn
					(setq vetor (diferencaPontos (nth (- (car menordistancia) 1) listaPontos) (nth (car menordistancia) listaPontos) ))
					(setq ponto (nth (car menordistancia) listaPontos) )
				)
				(progn
					(setq vetor (diferencaPontos (nth 1 listaPontos) (nth 0 listaPontos) ))
					(setq ponto (nth 0 listaPontos))
				)
			)
			(setq angulo (atan (cadr vetor) (car vetor)))

			(setq layeratual (getvar "clayer"))
			(setlayer layerDesenhar)
			(setq osnapvar (getvar "osmode"))
			(setvar "osmode" 0)
			(setq echoCurrent(getvar "cmdecho"))
			(setvar "CMDECHO" 0)
			(setq idcabo (atoi(car(legassoccabo plinha))))
			(command "_insert" nBloco ponto "" "" "" idcabo)
			(setq  lastEncabecamento (entlast))
			(setvar "clayer" layeratual)	
			(setvar "CMDECHO" echoCurrent)
			(setvar "osmode" osnapvar)

			(girartudo lastEncabecamento angulo)
			(alteraPntPlinha plinha ponto (cdr(assoc 10 (entget(entnext lastEncabecamento)))) ) ;
		)
	)
	
	(princ)
)
;===================================================================
(defun alteraPntPlinha(plinha pntI pntf / entidade saida contador elemento)
	(setq entidade (entget plinha))
	(setq saida (list))
	(setq contador 0)
	(while (setq elemento (nth contador entidade))
		(if (=(car elemento)10)
			(progn
				(if (= 0 (round (distance (cdr elemento) pntI) 0.01))
					(progn
						(setq saida(append saida (list (cons 10 pntf))))
					)
					(progn
						(setq saida(append saida (list elemento)))
					)
				)
			)
			(progn
				(setq saida(append saida (list elemento)))
			)
		)
		(setq contador (1+ contador))
	)
	(entmod saida)

)
;===================================================================
(defun C:Contarcabos ( / quantidadePorBloco listaNomesBlocos selecao tamanhoSelecao listaLinhas listaBlocos contador entidade posicao contadorLinhas saida tamanho)
		(setq quantidadePorBloco (list 0))
		(setq listaNomesBlocos (list 0))
		
		(setq selecao (ssget))
		(setq tamanhoSelecao (sslength selecao))
		(setq listaLinhas (ssadd))
		(setq listaBlocos (ssadd))
		;separar linhas de blocos
		(setq contador 0)
		(while (< contador tamanhoSelecao)
			(setq entidade (entget (ssname selecao contador)))
			(if (= "LINE" (cdr (assoc 0 entidade)))
				(progn
					(setq listaLinhas (ssadd (cdr (assoc -1 entidade)) listaLinhas))
				)
				(progn
					(if (= "INSERT" (cdr (assoc 0 entidade)))
						(progn
							(setq listaBlocos (ssadd (cdr (assoc -1 entidade)) listaBlocos))
						)
					)					
				)
			)
		(setq contador (+ 1 contador))
		)
		
		;calcular a distancia de um bloco a uma linha
		(setq tamanhoSelecaoBlocos (sslength listaBlocos))
		(setq contador 0)		
		(while (< contador tamanhoSelecaoBlocos)
		
			(setq entidade (entget (ssname listaBlocos contador)));seleciona o atual bloco
			;se o bloco é novo, adiciona-lo
			(if (not (setq posicao (PertenceLista (cdr(assoc 2 entidade)) listaNomesBlocos)))
				(progn 
					(setq listaNomesBlocos (append listaNomesBlocos  (list (cdr(assoc 2 entidade)))))
					(setq quantidadePorBloco (append quantidadePorBloco (list 0)))
					(setq posicao (- (length listaNomesBlocos) 1))
				)				
			)
			
			
			;percorre a lista de linhas para saber em qual lista ta o bloco		
			(setq tamanhoSelecaoLinhas (sslength listaLinhas))
			(setq contadorLinhas 0)			
			(while (< contadorLinhas tamanhoSelecaoLinhas) 				
				(setq linhaAtual (entget (ssname listaLinhas contadorLinhas)))
				(setq complinha (distance (cdr (assoc 10 linhaAtual)) (cdr (assoc 11 linhaAtual))))
				(setq distanciaAtual (distanciaLinhaPonto  (cdr(assoc 10 linhaAtual)) (cdr(assoc 11 linhaAtual)) (cdr (assoc 10 entidade))))
				(if (< distanciaAtual 0.1)
					(progn
						(setq quantidadePorBloco (settermolinha quantidadePorBloco posicao (+ complinha (nth posicao quantidadePorBloco))))
					)
				)
			(setq contadorLinhas (+ 1 contadorLinhas))
			)		
		(setq contador (+ 1 contador))
		
		)
		
		
		(setq saida " ")
		(setq tamanho (length  listaNomesBlocos))
		(setq contador 1)
		(while (< contador tamanho)
			(setq saida (strcat saida "\n"))
			(setq saida (strcat saida (nth contador  listaNomesBlocos)))
			(setq saida (strcat saida ": "))
			(setq saida (strcat saida (rtos (nth contador quantidadePorBloco) 2 2)))

			(setq contador (+ contador 1))
		)
		(alert saida)
		(princ)
	)
;==========================================================================================================
(defun distanciaPolinhaPonto(plinha ponto / contador menorDist )
	(setq listaPontos (getpontos plinha))
	
	(setq contador 0)
	(setq menorDist (distanciaLinhaPonto (nth 0 listaPontos) (nth 1 listaPontos) ponto))
	(while (and (not menorDist)(< contador (- (length listaPontos) 1)))
		(setq menorDist (distanciaLinhaPonto (nth contador listaPontos) (nth (+ contador 1) listaPontos) ponto))		
	(setq contador (+ contador 1))
	)
	
	(if menorDist
		(progn
			(setq contador 0)
			(while (< contador (- (length listaPontos) 1))
				(setq distanciaatual (distanciaLinhaPonto (nth contador listaPontos) (nth (+ contador 1) listaPontos) ponto))
				(if distanciaatual
					(progn
						(if (<= distanciaatual menorDist)
							(progn
								(setq menorDist distanciaatual)
							)
						)						
					)
				)
			(setq contador (+ contador 1))
			)
		)
	)
	menorDist
)

;==========================================================================================================
(defun posicionaLegenda(bloconome polilinha / vetor incpt ptlinha1 ptlinha2 a b ponto anguloInicialBloco a b pontos grr numpi)	
	
	(while (= 5 (car (setq grr (grread t 13 0))))
		(setq ponto (cadr grr))
		;getpontos seve para eliminar pontos que nao sao importantes, assim os calculos ficam mais rapidos
		(setq pontos (linhaMaisProxima (getpontos polilinha) ponto ))
		;pegar os pontos de inicio e fim da linha
        (if pontos
			(progn
			
				(setq ptlinha1 (nth 0 pontos))
				(setq ptlinha2 (nth 1 pontos))						

				(setq anguloInicialBloco (atan (- (cadr ptlinha1) (cadr ptlinha2)) (- (car ptlinha1) (car ptlinha2))))
				;girar o poste no angulo da linha
				(girarTudo bloconome anguloInicialBloco)	
				;pegar o vetor de referencia do poste, ele é o ponto do indentificador do poste em relacao ao ponto de incercao do poste
				(setq bloco (entget bloconome))
				
				(if(= (car ptlinha1) (car ptlinha2))
					(progn ;se a linha for vertical----------------------------------------------------------------------
						(if (> (car ponto) (car ptlinha1)); pra viar o bloco pra cima da linha
							(progn (girartudo bloconome (+ anguloInicialBloco 3.14159)))
							(progn (girartudo bloconome anguloInicialBloco))
						)
						(setq ponto (list (car ptlinha1) (cadr ponto) 0))							
						(redraw)
						(movetudo bloconome ponto)
					)
					(progn
						(if(= (cadr ptlinha1) (cadr ptlinha2))
							(progn ;se a linha for horizontal-------------------------------------------------------------------
								(if (> (cadr ponto) (cadr ptlinha1)); pra viar o bloco pra cima da linha
									(progn (girartudo bloconome (+ anguloInicialBloco 3.14159)))
									(progn (girartudo bloconome anguloInicialBloco))
								)
								(setq ponto (list (car ponto) (cadr ptlinha1) 0))							
								(redraw)
								(movetudo bloconome ponto)
							)
							(progn ; se nao é vertical nem horizontal--------------------------------------------------------------------
								;pegar os coeficientes a e b da equacao daquela linha
								(setq a (/ (- (cadr ptlinha1) (cadr ptlinha2))  (- (car ptlinha1) (car ptlinha2)) ))
								(setq b (-(cadr ptlinha1) (*(car ptlinha1) a)) )
				
								(if (> (cadr ponto) (valory a b (car ponto))); pra viar o bloco pra cima da linha
									(progn (girartudo bloconome (+ anguloInicialBloco 3.14159)))
									(progn (girartudo bloconome anguloInicialBloco))
								)
								(if (< (abs a) 1)
									(progn										
										(setq ponto (list (car ponto) (valory a b (car ponto)) 0))
									)
									(progn										
										(setq ponto (list (/(-(cadr ponto) b) a) (cadr ponto) 0))
									)
								)
								(redraw)
								(movetudo bloconome ponto)							
							)
						)
					)
				)
			)
		)                                                                                                        
	)
)
;===================================================================================================================
(defun colocarEncabecamentos (adesenhar plinha / contador lista)
	(setq contador 0)
	(setq lista (cdr (assoc 105 adesenhar)))
	(while (< contador (length lista))
			(if (= "baixa" (cadr(assoc 98 adesenhar)))
				(inserenaplinha plinha "encabecamento-existente" g_LayerCabosBaixa (atoi (nth contador lista)))
				(inserenaplinha plinha "encabecamento-existente" g_LayerCabosAlta (atoi (nth contador lista)))
			)
			
		(setq contador (1+ contador))
	)
)
;===================================================================================================================
(defun definecaminholevantamento (/ a saida contador adesenhar listaIN listaOUT )
	(setq saida objCabos)
	
	(if (and objPostes saida)
		(progn
			(setq contador 1)
			(while (setq adesenhar (cdr (assoc contador saida)))

				(setq listaIN (cdr (assoc 104 adesenhar)))
				(setq listaOUT (list))				
				(foreach a listaIN (progn (setq listaOUT (append listaOUT (list(assocAo (atoi a)))))))
				(if (listavalida listaOUT)
					(progn
						(setq listaOUT (mapcar 'itoa listaOUT))
						(setq adesenhar (subst (cons 104 listaOUT) (assoc 104 adesenhar) adesenhar))		
					)
				)				


				(setq listaIN (cdr (assoc 105 adesenhar)))
				(setq listaOUT (list))				
				(foreach a listaIN (progn (setq listaOUT (append listaOUT (list(assocAo (atoi a)))))))
				(if (listavalida listaOUT)
					(progn
						(setq listaOUT (mapcar 'itoa listaOUT))
						(setq adesenhar (subst (cons 105 listaOUT) (assoc 105 adesenhar) adesenhar))
					)
				)
				
				(setq saida (subst (cons contador adesenhar) (assoc contador saida) saida))
				(princ contador)
				(setq contador (+ contador 1))

			)
		)
	)
	saida
)
;===================================================================================================================
(defun listavalida (lista / saida a)
	(setq saida T)
	(foreach a lista (if (not a) (setq saida nil)))
	saida
)
;===================================================================================================================
(defun assocAo(num / saida quantidade contadorSS entityNome subentityNome texto )
	(setq saida nil)	
	(if (setq postesColocados (ssget "x" (list (cons 2  "CONTROLE_PONTO"))));selecionar todos os indetificadores
		(progn ; caso a selecao nao esteje vazia
			(setq quantidade (sslength postesColocados)); seta quantidade com o tamanho da selecao
			(setq contadorSS 0); crio um contador para a selecao, e coloco ele no comeco
			(while (< contadorSS quantidade); rodar enquanto o contador for menor que o tamanho da selecao
				(setq entityNome (ssname postesColocados contadorSS)); pegar o nome da "contador"iezima entidade da selecao
				(setq subentityNome entityNome)
				(while (/= (cdr( assoc 0 (entget(setq subentityNome (entnext subentityNome))))) "SEQEND"); procurar pelo atributo associado a entidade
					(if (and(= (cdr(assoc 0 (entget subentityNome))) "ATTRIB")(= (cdr(assoc 2 (entget subentityNome))) "IDlevantamento")) ; se uma das subentidades é um atributo, adicionar a lista
						(progn
							(setq texto (cdr (assoc 1 (entget subentityNome))))
							(if (= (atoi (car(explode 44 texto))) num)
								(setq saida (atoi (cadr(explode 44 texto))))
							)						
						);fim prog						
					); fim if
				); fim while
			(setq contadorSS (+ contadorSS 1)) ; somar um no contador para ir para a proxima entidade
			); fim while
		); fim progn
	); fim if
saida
)
;===================================================================================================================
(defun assocAoS(num / saida quantidade contadorSS entityNome subentityNome texto )
	(setq saida nil)	
	(if (setq postesColocados (ssget "x" (list (cons 2  "CONTROLE_PONTO"))));selecionar todos os indetificadores
		(progn ; caso a selecao nao esteje vazia
			(setq quantidade (sslength postesColocados)); seta quantidade com o tamanho da selecao
			(setq contadorSS 0); crio um contador para a selecao, e coloco ele no comeco
			(while (< contadorSS quantidade); rodar enquanto o contador for menor que o tamanho da selecao
				(setq entityNome (ssname postesColocados contadorSS)); pegar o nome da "contador"iezima entidade da selecao
				(setq subentityNome entityNome)
				(while (/= (cdr( assoc 0 (entget(setq subentityNome (entnext subentityNome))))) "SEQEND"); procurar pelo atributo associado a entidade
					(if (and(= (cdr(assoc 0 (entget subentityNome))) "ATTRIB")(= (cdr(assoc 2 (entget subentityNome))) "IDlevantamento")) ; se uma das subentidades é um atributo, adicionar a lista
						(progn
							(setq texto (cdr (assoc 1 (entget subentityNome))))
							(if (= (atoi (cadr(explode 44 texto))) num)
								(setq saida (atoi (car(explode 44 texto))))
							)						
						);fim prog						
					); fim if
				); fim while
			(setq contadorSS (+ contadorSS 1)) ; somar um no contador para ir para a proxima entidade
			); fim while
		); fim progn
	); fim if
saida
)
;===================================================================================================================
(defun colocaCabos(Levantamento / adesenhar contador Plinha legendas)
	(setq contador 1)
		(while (setq adesenhar (cdr (assoc contador levantamento)))
			(princ "\nCabo ")
			(princ contador)
			(if (not (caboEntNome contador))
				(progn
					(setq Plinha (insereCabo adesenhar))					
					(setq legendas (insereLegendaCabo Plinha adesenhar))
					(colocarEncabecamentos adesenhar plinha)
					(initget "Continuar Parar")
					(setq resposta (cond ( (getkword "\nEnter p/ continuar... [Continuar/Parar] <Continuar>: ") ) ( "Continuar" )))
					(if (= resposta "Parar")
						(progn (exit))
					)	
					(princ "\nCabo ")
					(princ contador)
				)
			)
			(princ "- Presente")			
			(setq contador (+ contador 1))
		)

	(alert "\nTodos os Cabos foram colocados")
)
;=====================================================================================================================
(defun insereLegendaCabo (Plinha adesenhar / ponto texto layeratual lasttabela)
	(setq ponto '(0 0 0))
	(setq texto(strcat (cadr (assoc 99 adesenhar)) "#" (cadr (assoc 100 adesenhar))))
	(if (assoc 101 adesenhar) 
		(setq texto(strcat texto "("  (cadr (assoc 101 adesenhar)) ")" ))
	)
	(setq texto(strcat texto (cadr (assoc 102 adesenhar))))
	(setq saida (ssadd))
	(if (= "baixa" (cadr(assoc 98 adesenhar)))
		(progn
			(setq layeratual (getvar "clayer"))
			(setlayer g_LayerCabosBaixa)
			(command "_insert" g_textoCabo ponto "1" "1" "0" texto (cadr (assoc 97 adesenhar)) )
			(setq lasttabela (entlast))
			(setvar "clayer" layeratual)
			(posicionaLegenda lasttabela Plinha)
			(setq saida (ssadd lasttabela saida))
			(while (or (initget "Continuar +Legenda") (/= "Continuar" (setq resposta (cond ( (getkword "\nEnter p/ continuar... [Continuar/+Legenda] <Continuar>: ") ) ( "Continuar" )))))
				(if  (= "+Legenda" resposta)
					(progn 
						(setq layeratual (getvar "clayer"))
						(setlayer g_LayerCabosBaixa)
						(command "_insert" g_textoCabo ponto "1" "1" "0" texto (cadr (assoc 97 adesenhar)) )
						(setq lasttabela (entlast))
						(setvar "clayer" layeratual)
						(posicionaLegenda lasttabela Plinha)
						(setq saida (ssadd lasttabela saida))
					)
				)		
			)		
		)
		(progn
			(setq texto(strcat texto " - "(cadr (assoc 103 adesenhar))))
			(setq layeratual (getvar "clayer"))
			(setlayer g_LayerCabosAlta)
			(command "_insert" g_textoCabo ponto "1" "1" "0" texto (cadr (assoc 97 adesenhar)))
			(setq lasttabela (entlast))
			(setvar "clayer" layeratual)
			(posicionaLegenda lasttabela Plinha)
			(setq saida (ssadd lasttabela saida))
			(while (or (initget "Continuar +Legenda") (/= "Continuar" (setq resposta (cond ( (getkword "\nEnter p/ continuar... [Continuar/+Legenda] <Continuar>: ") ) ( "Continuar" )))))
				(if  (= "+Legenda" resposta)
					(progn 
						(setq layeratual (getvar "clayer"))
						(setlayer g_LayerCabosAlta)
						(command "_insert" g_textoCabo ponto "1" "1" "0" texto (cadr (assoc 97 adesenhar)))
						(setq lasttabela (entlast))
						(setvar "clayer" layeratual)
						(posicionaLegenda lasttabela Plinha)
						(setq saida (ssadd lasttabela saida))
					)
				)		
			)
		)
	)
)
;=====================================================================================================================
(defun insereCabo(adesenhar / angulofinal grr anguloInicial numpi listaPontos caminhoCabo circuloReferencia circuloReferenciaNome ponto saida contador pontoInicial pontoFinal posteEnt pntIncersao vetor modulo)
	(setq numpi 3.14159265359)
	
	(setq caminhoCabo (cdr(assoc 104 adesenhar)))
	
	(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext(posteEntNome (atoi (nth 0 caminhoCabo))))))) (cdr(assoc 10 (entget(posteEntNome (atoi (nth 0 caminhoCabo))))))))
	(setq modulo (car(cartParaPolar vetor)))
			
	(setq listaPontos (list))
	(setq contador 0)
	(if (= "baixa" (cadr(assoc 98 adesenhar)))
		(progn
			(while (< contador (* 2 (length caminhoCabo)))
				(setq posteEnt (entget (posteEntNome (atoi (nth (/ contador 2) caminhoCabo)))))
				
				(if (or (= g_BposteDTExistente (cdr (assoc 2 posteEnt))) (= g_BposteDTInstalar (cdr (assoc 2 posteEnt))))
					(progn ;para poste DT	
						(setq pntIncersao (cdr(assoc 10 posteEnt)))
						(setq vetor (diferencaPontos (cdr (assoc 10 (entget(entnext (cdr(assoc -1 posteEnt)))))) pntIncersao ))
						(setq anguloInicial (cadr (cartParaPolar vetor)))
			
						(setq circuloReferencia (desenhaCirculo (somaPontos pntIncersao vetor) (/ (car(cartParaPolar vetor)) 4) (car g_LayerAcessorios)))
						(setq circuloReferenciaNome (entget circuloReferencia))
						(setq numpi 3.14159265359)
						
						(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved	
							(setq ponto (cadr grr))			
							(setq ponto (diferencaPontos ponto pntIncersao))
							(setq ponto (rotVetor ponto (- (* -1 anguloInicial)(/ numpi 8))))
								
							(setq angulofinal 0)
							(setq modulofinal (car(cartParaPolar vetor)))
							(setq angRelativo (atan (cadr ponto) (car ponto)))
							
							
							(if (and (> angRelativo 0) (< angRelativo (* pi 0.25)))
								(progn
									(setq angulofinal (+ anguloInicial (* numpi 0.25)))
									(setq modulofinal (* modulofinal (sqrt 2)))
								)
								(progn
									(if (and (> angRelativo (* pi 0.25)) (< angRelativo (* pi 0.5)))
										(progn
											(setq angulofinal (+ anguloInicial(* numpi 0.5)))						
										)
										(progn
											(if (and (> angRelativo (* pi 0.5)) (< angRelativo (* pi 0.75)))
												(progn
													(setq angulofinal (+ anguloInicial(* numpi 0.75)))
													(setq modulofinal (* modulofinal (sqrt 2)))
												)
												(progn
													(if (and (> angRelativo (* pi 0.75)) (< angRelativo (* pi 1)))
														(progn
															(setq angulofinal (+ anguloInicial(* numpi 1)))
															
														)
														(progn
															(if (and (< angRelativo (* pi 0)) (> angRelativo (* pi -0.25)))
																(progn
																	(setq angulofinal (+ anguloInicial (* numpi 0)))
																)
																(progn
																	(if (and (< angRelativo (* pi -0.25)) (> angRelativo (* pi -0.5)))
																		(progn
																			(setq angulofinal (+ anguloInicial(* numpi -0.25)))
																			(setq modulofinal (* modulofinal (sqrt 2)))
																		)
																		(progn
																			(if (and (< angRelativo (* pi -0.5)) (> angRelativo (* pi -0.75)))
																				(progn
																					(setq angulofinal (+ anguloInicial(* numpi -0.5)))
																				)
																				(progn
																					(if (and (< angRelativo (* pi -0.75)) (> angRelativo (* pi -1)))
																						(progn
																							(setq angulofinal (+ anguloInicial(* numpi -0.75)))
																							(setq modulofinal (* modulofinal (sqrt 2)))
																						)
																					)
																				)
																			)						
																		)
																	)	
																)
															)
														)
													)
												)
											)						
										)
									)	
								)
							)

							(setq pontoFinal (list modulofinal angulofinal))
							(setq pontoFinal (polarParaCart pontoFinal))
							(setq pontofinal (somaPontos pntIncersao pontoFinal))					
								
							(redraw)
							(setq circuloReferenciaNome (subst (cons 10 pontofinal) (assoc 10 circuloReferenciaNome) circuloReferenciaNome))
							(entmod circuloReferenciaNome)
							)				
						
						(entdel circuloReferencia)
					)
					(progn ;para poste CC
						(setq pntIncersao (cdr(assoc 10 posteEnt)))
						(setq vetor (diferencaPontos (cdr (assoc 10 (entget(entnext (cdr(assoc -1 posteEnt)))))) pntIncersao ))
						(setq circuloReferencia (desenhaCirculo (somaPontos pntIncersao vetor) (/ (car(cartParaPolar vetor)) 4) (car g_LayerAcessorios)))
						(setq circuloReferenciaNome (entget circuloReferencia))
							
						(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved				
							(setq ponto (cadr grr))					
							(setq ponto (diferencaPontos ponto pntIncersao))
							(setq angulofinal (cadr (cartParaPolar ponto)))
							
							(setq pontoFinal (cartParaPolar vetor))
							(setq pontoFinal (list (car pontoFinal) angulofinal))
							(setq pontoFinal (polarParaCart pontoFinal))
							
							(setq pontofinal (somaPontos pntIncersao pontoFinal))
							(redraw)
							(setq circuloReferenciaNome (subst (cons 10 pontoFinal) (assoc 10 circuloReferenciaNome) circuloReferenciaNome))
							(entmod circuloReferenciaNome) 
						)
						(entdel circuloReferencia)
						
					)
				)		
				(setq listaPontos (append listaPontos (list Pontofinal)))	
				(if (pertencelista (nth (/ contador 2) caminhoCabo) (cdr(assoc 105 adesenhar)))
					(setq contador (+ contador 1))
				)
				(setq contador (+ contador 1))
			)
			(setq layeratual (getvar "clayer"))
			(setlayer g_LayerCabosBaixa)
			(setq osnapvar (getvar "osmode"))
			(setvar "osmode" 0)
			(setq echoCurrent(getvar "cmdecho"))
			(setvar "CMDECHO" 0)
		)
		(progn
			(while (< contador (length caminhoCabo))
				(setq posteEnt (entget (posteEntNome (atoi (nth contador caminhoCabo)))))
				(setq pntIncersao (cdr(assoc 10 posteEnt)))
				(setq pontoFinal pntIncersao)	
				(setq listaPontos (append listaPontos (list Pontofinal)))
				(setq contador (+ contador 1))
			)
			
			; (setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext(posteEntNome (atoi (nth 0 caminhoCabo))))))) (cdr(assoc 10 (entget(posteEntNome (atoi (nth 0 caminhoCabo))))))))
			; (setq modulo (car(cartParaPolar vetor)))
			(if (cdr(assoc 105 adesenhar)) 
				(progn
					(if (pertencelista  (car caminhoCabo) (cdr(assoc 105 adesenhar)) )
						(progn
							(setq vetor (diferencaPontos (car listaPontos)(cadr listaPontos)))
							(setq vetor (cartParaPolar vetor))
							(setq vetor (list (- (car vetor) modulo) (cadr vetor) ))
							(setq vetor (polarParaCart vetor))
							
							(setq listaPontos (settermolinha listaPontos 0 (somaPontos vetor (cadr listaPontos))))
						)
					)
					(if (pertencelista (nth (- (length caminhoCabo)1) caminhoCabo) (cdr(assoc 105 adesenhar)) )
						(progn			
							(setq vetor (diferencaPontos (nth (- (length listaPontos)1) listaPontos)(nth (- (length listaPontos) 2) listaPontos)))
							(setq vetor (cartParaPolar vetor))
							(setq vetor (list (- (car vetor) modulo) (cadr vetor) ))
							(setq vetor (polarParaCart vetor))
							
							(setq listaPontos (settermolinha listaPontos (- (length listaPontos)1) (somaPontos vetor (nth (- (length listaPontos) 2) listaPontos))))
						)
					)
				)
			)
			
			
			
			(setq layeratual (getvar "clayer"))
			(setlayer g_LayerCabosAlta)
			(setq osnapvar (getvar "osmode"))
			(setvar "osmode" 0)
			(setq echoCurrent(getvar "cmdecho"))
			(setvar "CMDECHO" 0)
		)
	)	
	
	(if (not(pertencelista  (car caminhoCabo) (cdr(assoc 105 adesenhar)) ))
		(progn
			(setq vetor (diferencaPontos (car listaPontos)(cadr listaPontos)))
			(setq vetor (cartParaPolar vetor))
			(setq vetor (list (+ (car vetor) (* 10 modulo)) (cadr vetor) ))
			(setq vetor (polarParaCart vetor))
			(setq listaPontos (append (list(somaPontos vetor (cadr listaPontos))) listaPontos ))
		)
	)


	(if (not(pertencelista (nth (- (length caminhoCabo)1) caminhoCabo) (cdr(assoc 105 adesenhar)) ))
		(progn			
			(setq vetor (diferencaPontos (nth (- (length listaPontos)1) listaPontos)(nth (- (length listaPontos) 2) listaPontos)))
			(setq vetor (cartParaPolar vetor))
			(setq vetor (list (+ (car vetor) (* 10 modulo)) (cadr vetor) ))
			(setq vetor (polarParaCart vetor))
			(setq listaPontos (append listaPontos (list (somaPontos vetor (nth (- (length listaPontos) 2) listaPontos)))))
		)
	)
	
	
	
	
	
	
    (command "_Pline")
	(setq contador 0)
	(while (< contador (length listaPontos))
		(command (nth contador listaPontos))
		(setq contador (+ contador 1))
	)
	(command "")
    (setvar "CMDECHO" echoCurrent)
	(setvar "osmode" osnapvar)
	(setvar "clayer" layeratual)
	(entlast)
)
;====================================================================================================
(defun desenhaLinha(p1 p2 layerNome)
	(entmake
	   (list  
			(cons 0 "LINE") 
			(cons 10 p1) 
			(cons 11 p2) 
			(cons 8 layerNome) 
			(cons 210 (list 0.0 0.0 1.0)) 
		)
	)
	(entlast)
)
;====================================================================================================
(defun desenhaCirculo(p1 raio layerNome)
	(entmake
	   (list (cons 0 "CIRCLE")
             (cons 10 p1)
             (cons 40 raio)
			 (cons 8 layerNome) 
		)
	)
	(entlast)
)
;=======================================================================================================================================================
(defun posteEntNome (ID / entityNome subentityNome saida postesCCInstalados postesDTInstalados todosPostes contadorCC quantSelCC quantSel)
	(setq saida nil)
	
	(setq postesDTExistente (ssget "x" (list (cons 2 g_BposteDTExistente))))
	(setq postesCCExistente (ssget "x" (list (cons 2  g_BposteCCExistente))))
	(setq postesDTInstalar (ssget "x" (list (cons 2 g_BposteDTInstalar))))
	(setq postesCCInstalar (ssget "x" (list (cons 2  g_BposteCCInstalar))))

	(setq todosPostes postesDTExistente)
	(if postesCCExistente
		(progn
			(setq contadorCC 0)
			(setq quantSelCC(sslength postesCCExistente))
			(while (< contadorCC quantSelCC)
				(setq todosPostes (ssadd (ssname postesCCExistente contadorCC) todosPostes))
				(setq contadorCC (+ contadorCC 1))
			)
		)
	)
	
	(if postesDTInstalar
		(progn
			(setq contadorCC 0)
			(setq quantSelCC(sslength postesDTInstalar))
			(while (< contadorCC quantSelCC)
				(setq todosPostes (ssadd (ssname postesDTInstalar contadorCC) todosPostes))
				(setq contadorCC (+ contadorCC 1))
			)
		)
	)

	(if postesCCInstalar
		(progn
			(setq contadorCC 0)
			(setq quantSelCC(sslength postesCCInstalar))
			(while (< contadorCC quantSelCC)
				(setq todosPostes (ssadd (ssname postesCCInstalar contadorCC) todosPostes))
				(setq contadorCC (+ contadorCC 1))
			)
		)
	)
	(setq quantSel(sslength todosPostes))
	(setq contadorCC 0)
	
	(while (< contadorCC quantSel)
		(setq entityNome (ssname todosPostes contadorCC))
		(setq subentityNome entityNome)
		(while (/= (cdr( assoc 0 (entget(setq subentityNome (entnext subentityNome))))) "SEQEND")
			(if (= (cdr(assoc 0 (entget subentityNome))) "ATTRIB")
				(progn
					(if (= (atoi (cdr (assoc 1 (entget subentityNome)))) ID)
						(progn 
							(setq saida entityNome)
							(if (= (round (distance (list 0 0 0) (cdr (assoc 10 (entget saida)))) 0.01) 0)(alert "poste na origem, isso pode ser um defeito."))
						)
					)
				)
			)
		)
		(setq contadorCC (+ contadorCC 1))
	)

	
	saida

)
;==========================================================================================================

;==========================================================================================================
(defun caboEntNome (ID / contadordeLegCabos subentLeg entLeg contador Polilinhas LegCabos saida flag distanciaCabolinha entityNome)
	(setq saida (list))
	(setq flag T)
	
	(setq LegCabos (ssget "_X" (list (cons 0 "INSERT") (cons 2  g_textoCabo))))
	(setq Polilinhas (ssget "_X" (list (cons 0 "LWPOLYLINE"))))

	
	(if (and polilinhas LegCabos)
		(progn
			
			(setq quantCabos(sslength Polilinhas))
			(setq contador 0)
			
			(while (and (< contador quantCabos) flag); flag serve para quando achar a que estou procurando interromper o loop
				(setq entityNome (ssname Polilinhas contador))
				(setq contadordeLegCabos 0)
				(while (and (< contadordeLegCabos (sslength LegCabos))
					(/= 0 (setq distanciaCabolinha (round (distanciaPolinhaPonto entityNome (cdr (assoc 10 (entget(setq entLeg (ssname LegCabos contadordeLegCabos)))))) 0.1))))
					(setq contadordeLegCabos (1+ contadordeLegCabos))
				)

				(if (= distanciaCabolinha 0)
					(progn
						(setq subentLeg (entnext entLeg))
						(while (= "ATTRIB" (cdr (assoc 0 (entget subentLeg))))
							(if (=  (cdr (assoc 1 (entget subentLeg))) (itoa ID))
								(progn
									(setq flag nil)
									(setq saida (list entityNome entLeg))
								)
							)
							(setq subentLeg (entnext subentLeg))		
						)
					)
				)		
				(setq contador (1+ contador))
			)
		)
	)
	
	saida
)

;==================================================================================================================
(defun mudaPonto(linha1 linha2 ponto / entlinha1 entlinha2 aux)
	(setq entlinha1 (entget linha1))
	(setq entlinha2 (entget linha2))
	(if (= (cdr (assoc 10 entlinha1)) (cdr(assoc 11 entlinha2))) 
		(progn
			(setq aux entlinha1)
			(setq entlinha1 entlinha2)
			(setq entlinha2 aux)
		)
	)
	(setq entlinha1 (subst (cons 11 ponto) (assoc 11 entlinha1) entlinha1))
	(setq entlinha2 (subst (cons 10 ponto) (assoc 10 entlinha2) entlinha2))
	(entmod entlinha1)
	(entmod entlinha2)
)
;==================================================================================================================
(princ)