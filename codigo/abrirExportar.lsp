;==================================================================================
(defun carregaLevantamento( locale / quantidadepostes contador nomearquivo saida flag listaObj id linha)
	(setq quantidadepostes 0)
	(if (setq nomearquivo(open locale "r"))
			(progn
				(while  (/= (setq id (read-char nomearquivo)) 33)
					(if (= id 45)
						(setq quantidadepostes (+ quantidadepostes 1))						
					)
					(read-line nomearquivo)
				)
				(close nomearquivo)
			)
	)

	(if (setq nomearquivo(open locale "r"))
		(progn
			(setq contador 1)
			(while (<= contador quantidadepostes)
				(setq listaObj nil)
				(while (/= (setq id (read-char nomearquivo)) 45)
					(read-char nomearquivo)
					(setq linha(read-line nomearquivo))
					(setq linha (explode 32 linha))
					(setq listaObj (append listaObj (list (cons id  linha))))
				)
				(read-line nomearquivo)
				(setq saida (append saida (list (cons contador  listaObj))))
			(setq contador (+ contador 1))
			)
			(close nomearquivo)
			saida
		)
	)
)
;=================================================================================
(defun openConf( / linha nomearquivo endereco contador TAG VALOR lista linha)
(setq endereco "C:\\Users\\Daniel\\Desktop\\autolisp-versao2\\codigo\\variaveis.conf")
(setq TAG "")
(setq VALOR "")


(setq g_layerPostes (list "layerdospostes" "161"))
(setq g_layerPontosInst (list "layerdospontosinstalar" "7"))
(setq g_layerTabelas (list "layerdastabelas" "212"))
(setq g_LayerAcessorios (list "layerdosacessorios" "140"))
(setq g_LayerCabosBaixa (list "layerdosCabosBaixa" "102"))
(setq g_LayerCabosAlta (list "layerdosCabosAlta" "5"))
(setq g_layerVetores (list "layerdosVetores" "4"))

(setq g_textoCabo "LEG_CABO")
(setq g_Btabela4 "ID_POSTE4")
(setq g_Btabela3 "ID_POSTE3")
(setq g_BtabelaTrafo "ID_TRAFO3")
(setq g_BposteDTExistente "POSTE-DT-EXISTENTE")
(setq g_BposteCCExistente "POSTE-CC-EXISTENTE")
(setq g_BposteDTInstalar "POSTE-DT-INSTALAR")
(setq g_BposteCCInstalar "POSTE-CC-INSTALAR")
(setq g_BiluminacaoExis "ILUMINARIA-EXISTENTE")
(setq g_BiluminacaoInst "ILUMINARIA-INSTALAR")
(setq g_BpararaioExistente "PARA-RAIO-EXISTENTE")
(setq g_BpararaioInstalar "PARA-RAIO-INSTALAR")
(setq g_BtrafoExistente "TRAFO-EXISTENTE")
(setq g_BtrafoInstalar "TRAFO-INSTALAR")
(setq g_BaterramentoExistente "ATERRAMENTO-EXISTENTE")
(setq g_BaterramentoInstalar "ATERRAMENTO-INSTALAR")
(setq g_BchaveFusivelExis "CHAVE-FUSIVEL-EXISTENTE")
(setq g_BchaveFusivelInst "CHAVE-FUSIVEL-INSTALAR")

;--------- CABO ------------- tracao de projeto (daN) - Corrente (a) - Peso (daN/m) - Tracao de ruptura (daN)
(setq g_4AWC/CA (list 					61      		 101             0.0584                  393              ))
(setq g_2AWC/CA (list 					89  		     138             0.0927                  602              ))
(setq g_1/0AWC/CA (list 				142 		     184             0.1475                  883              ))
(setq g_2/0AWC/CA (list 				159  		     220             0.185                   1113             ))
(setq g_4/0AWC/CA (list 				284  		     293             0.2956                  1726             ))
(setq g_336.4CA (list 	   	 			452   		 395             0.4701                  2813             ))

(setq g_50mmXLPE (list 	   	 		398   		 208             0.26     ))
(setq g_95mmXLPE (list 	   	 		449         	 313             0.455    ))
(setq g_150mmXLPE (list 	   	 		503      		 415             0.585    ))




	(if (setq nomearquivo(open endereco "r"))
		(progn
			(while (setq linha (read-line nomearquivo))
				(setq lista (vl-string->list linha))				
				(if (>(length lista) 5)
					(progn				
						(if (/= 37 (nth 0 lista))
							(progn
								(setq TAG (stringAntesde 61 linha))				
								(setq VALOR (stringEntre 34 linha))		
								(if (= TAG "Bloco Postes Existentes ")
									(setq g_BposteExistente VALOR)
								)
								(if (= TAG "layer Postes ")
									(setq g_layerPostes (list VALOR (cadr g_layerPostes)))
								)
								(if (= TAG "CorLayerPostes ")
									(setq g_layerPostes (list (car g_layerPostes) VALOR))
								)
								(if (= TAG "Bloco Tabela Postes 3 linhas ")
									(setq g_Btabela3 VALOR)
								)
								(if (= TAG "Bloco Tabela Postes 4 linhas ")
									(setq g_Btabela4 VALOR)
								)
								(if (= TAG "Bloco Tabela TRAFO ")
									(setq g_BtabelaTrafo VALOR)
								)
								(if (= TAG "layerTabela ")
									(setq g_layerTabelas (list VALOR (cadr g_layerTabelas)))
								)				
								(if (= TAG "CorLayerTabelas ")
									(setq g_CorLayerTabelas (list (car g_layerTabelas) VALOR))
								)		
								(if (= TAG "Bloco Iluminacao ")
									(setq g_Biluminacao VALOR)
								)							
								(if (= TAG "Bloco Para raio Existente ")
									(setq g_Bpararaio VALOR)
								)								
								(if (= TAG "Bloco Transformador ")
									(setq g_BtrafoExistente VALOR)
								)
								(if (= TAG "Bloco Aterramento ")
									(setq g_Baterramento VALOR)
								)								
								(if (= TAG "Bloco Chave fusivel ")
									(setq g_BchaveFusivel VALOR)
								)								
								(if (= TAG "layerAcessorios ")
									(setq g_LayerAcessorios (list VALOR (cadr g_LayerAcessorios)))
								)
								(if (= TAG "CorLayerAcessorios ")
									(setq g_CorLayerAcessorios (list (car g_LayerAcessorios) VALOR))
								)
								
							)
						)					
					)
				)
			)
			(close nomearquivo)
		)		
	)
	(princ)
)

;==================================================================================
(defun explode(caractere frase / saida posicaoInicial posicaofinal)
	(setq saida  (list))
	(setq posicaoInicial 1)
	(while (/= nil (setq posicaofinal (vl-string-position caractere frase posicaoInicial)))
		(setq saida (append saida (list (substr frase posicaoInicial  (- posicaofinal posicaoinicial -1)))))
		(setq posicaoInicial (+ posicaofinal 2))
	) 
	(setq saida (append saida (list  (substr frase posicaoInicial))))
)
;=============================================================================
(defun stringAntesde(caractere frase / posicaofinal)
	(setq posicaofinal (vl-string-position caractere frase))
	(substr frase 1  posicaofinal)
)
;=============================================================================
(defun stringEntre(caractere frase / saida posicaoInicial posicaofinal)
	(setq posicaoInicial (+ 2 (vl-string-position caractere frase)))
	(setq posicaoFinal (vl-string-position caractere frase posicaoInicial))
	(substr frase posicaoInicial  (- posicaofinal posicaoinicial -1))
)
;=============================================================================
(defun stringEntreD(caractere1 caractere2 frase / saida posicaoInicial posicaofinal)
	(setq posicaoInicial (vl-string-position caractere1 frase))
	(if posicaoInicial
		(progn
			(setq posicaoInicial (+ 2 posicaoInicial))
			(setq posicaofinal (vl-string-position caractere2 frase posicaoInicial))
			(if (and posicaoFinal posicaoInicial)
				(substr frase posicaoInicial  (- posicaofinal posicaoinicial -1))
				nil
			)
		)
		nil
	)
		
)
;=============================================================================

(princ)
;










