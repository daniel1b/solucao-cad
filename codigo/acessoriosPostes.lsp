;===============================================================================================================================================
(defun insereID(adesenhar / lastID)
		(setq ponto (list 5 '(0 0 0)))
		(setq ID (cadr(assoc 97 adesenhar)))
		(setq layeratual (getvar "clayer"))
		(setlayer g_LayerAcessorios)
		(command "_insert" "ID_PONTO" (cadr ponto) "" "" "" ID)
		(setq  lastID (entlast))
		(setvar "clayer" layeratual)
		
		(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
			(setq ponto (cadr grr))
			(redraw)
			(moveTabela lastID ponto)
		)
		
		lastID
)
;===============================================================================================================================================
(defun inserecontrole (adesenhar poste sequencia / controle)
		(setq ponto (cdr(assoc 10 (entget poste))))
		(setq ID (itoa sequencia))
		(setq IDlevantamento (cadr(assoc 97 adesenhar)))
		(setq layeratual (getvar "clayer"))
		(setlayer g_LayerAcessorios)
		(command "_insert" "CONTROLE_PONTO" ponto "" "" "" (strcat ID "," IDlevantamento) IDlevantamento)
		(setq  lastID (entlast))
		(setvar "clayer" layeratual)	
		
		controle
)
;------------------------------------------------------------------------------------------------------------------------
(defun corrigirPontoExistente (obj / ddiag dcl_id saida id tipo res baixa media indent potLamp pararaio fusivel aterramento trafo salvaedicoes poste saida)
	(setq poste (cadr(assoc 98 obj)))
	(if(not(setq dcl_id (load_dialog "corrigirPntExistente.dcl")))
		(progn
		  (alert "Problema ao carregar corrigirPntExistente.dcl")
		  (exit)
		)
		(progn
		  (if (not (new_dialog "corrigeExistente" dcl_id))
			(progn
			  (alert "Problema ao carregar a definicao principal do dcl")
			  (exit)
			)
			(progn
				  (action_tile "cancel" "(done_dialog 1)")			  
				  (action_tile "accept" "(salvaedicoes )(done_dialog 2)") 
				  (action_tile "ultimo" "(set_tile \"sequencia\" (itoa(+ 1 (cond ((getultimoID)) (0)))))")
				  ;----------------------------------------------------------------------------------------------------------------------------------------
				 (defun salvaedicoes( )
					(setq saida (list))
					
					(setq id (get_tile "sequencia"))
					(setq tipo (atoi (get_tile "dt")))
					(setq res (get_tile "resistenciatamanho"))
					(setq baixa (get_tile "estruturaBT"))
					(setq media (get_tile "estruturaMT"))
					(setq indent (get_tile "numeroposte"))
					(setq potLamp (get_tile "iluminacao"))
					(setq pararaio (atoi(get_tile "pararaio")))
					(setq fusivel (atoi(get_tile "fusivel")))
					(setq aterramento (atoi(get_tile "aterramento")))
					(setq trafo (atoi(get_tile "trafo")))
					
					(setq saida (append saida (list (cons 97 (list id)))))
					(if (= tipo 1) (setq saida (append saida (list (cons 98 (list "dt")))))(setq saida (append saida (list (cons 98 (list "cc"))))))
					(setq saida (append saida (list (cons 99 (list res)))))
					(if (/= baixa "") (setq saida (append saida (list (cons 100 (list baixa))))))
					(if (/= media "") (setq saida (append saida (list (cons 101 (list media))))))
					(if (= indent "") (setq saida (append saida (list (cons 102 (list "ilegivel"))))) (setq saida (append saida (list (cons 102 (list indent))))))
					(if (/= potLamp "") (setq saida (append saida (list (cons 103 (list potLamp))))))
					(if (= trafo 1) (setq saida (append saida (list (cons 106 (explode 32 (get_tile "posto")))))))
					(if (= pararaio 1) (setq saida (append saida (list (cons 104 (list "pararaio"))))))
					(if (= fusivel 1) (setq saida (append saida (list (cons 105 (list "chave-fusivel"))))))
					(if (= aterramento 1) (setq saida (append saida (list (cons 107 (list "aterramento"))))))
				)
				;-----------------------------------------------------------------------------------------------------------------------------------------------------
					  (set_tile "resistenciatamanho" (cadr (assoc 99 obj)))   
					  (set_tile "numeroposte" (cadr (assoc 102 obj)))
					  (set_tile "sequencia" (cadr (assoc 97 obj)))
					 
					  
					  
					  (if  (cdr (assoc 106 obj))
							(set_tile "posto" (strcat (cadr (assoc 106 obj)) " " (caddr (assoc 106 obj)) " " (cadddr (assoc 106 obj))))
							(set_tile "posto" " ") 
					  )
					  
					  (if  (cadr (assoc 103 obj))
							(set_tile "iluminacao" (cadr (assoc 103 obj))) 
							(set_tile "iluminacao" "") 
					  )
						
					  (if (assoc 100 obj)
							 (set_tile "estruturaMT" (cadr (assoc 101 obj)))
							 (set_tile "estruturaMT" "")
					  )
					  
					  (if (assoc 101 obj)
							 (set_tile "estruturaBT" (cadr (assoc 100 obj)))
							 (set_tile "estruturaBT" "")
					  )
					  
					  (if (assoc 107 obj)
							(set_tile "aterramento" "1")
							(set_tile "aterramento" "0")
					  )
					  
					  (if (assoc 104 obj)
							(set_tile "pararaio" "1")
							(set_tile "pararaio" "0")
					  )
					  
					  (if (assoc 105 obj)
							(set_tile "fusivel" "1")
							(set_tile "fusivel" "0")
					  )
					  
					  (if (assoc 106 obj)
							(set_tile "trafo" "1")
							(set_tile "trafo" "0")
					  )

					  
					  (if (= poste "dt")
							(set_tile "dt" "1")
					  )
					  (if (= poste "cc")
							(set_tile "cc" "1")
					  )
					  
					  (setq ddiag(start_dialog)) 
					  (unload_dialog dcl_id)
					  
					  (if (= ddiag 1)
						(progn
							(princ "\nsaindo...")
							(exit)
						)						
					  )
					  (if (= ddiag 2)
						(progn
							(princ "\nColocar ponto")
						)						
					 )
				)
			)
		)
	)	
	saida
)
;=============================================================================================================================
(defun corrigirPontoInstalar (obj / ddiag dcl_id saida id tipo res baixa media indent potLamp pararaio fusivel aterramento trafo salvaedicoes poste saida)
	(setq poste (cadr(assoc 98 obj)))
	(if(not(setq dcl_id (load_dialog "corrigirPntInstalar.dcl")))
		(progn
		  (alert "Problema ao carregar corrigirPntInstalar.dcl")
		  (exit)
		)
		(progn
		  (if (not (new_dialog "corrigeInstalar" dcl_id))
			(progn
			  (alert "Problema ao carregar a definicao principal do dcl")
			  (exit)
			)
			(progn
				  (action_tile "cancel" "(done_dialog 1)")			  
				  (action_tile "accept" "(salvaedicoes )(done_dialog 2)") 
				  (action_tile "ultimo" "(set_tile \"sequencia\" (itoa(+ 1 (cond ((getultimoID)) (0)))))")
				  ;----------------------------------------------------------------------------------------------------------------------------------------
				 (defun salvaedicoes( )
					(setq saida (list))
					
					(setq id (get_tile "sequencia"))
					(setq tipo (atoi (get_tile "dt")))
					(setq res (get_tile "resistenciatamanho"))
					(setq baixa (get_tile "estruturaBT"))
					(setq media (get_tile "estruturaMT"))
					(setq potLamp (get_tile "iluminacao"))
					(setq pararaio (atoi(get_tile "pararaio")))
					(setq fusivel (atoi(get_tile "fusivel")))
					(setq aterramento (atoi(get_tile "aterramento")))
					(setq trafo (atoi(get_tile "trafo")))
					
					(setq saida (append saida (list (cons 97 (list id)))))
					(if (= tipo 1) (setq saida (append saida (list (cons 98 (list "dt")))))(setq saida (append saida (list (cons 98 (list "cc"))))))
					(setq saida (append saida (list (cons 99 (list res)))))
					(if (/= baixa "") (setq saida (append saida (list (cons 100 (list baixa))))))
					(if (/= media "") (setq saida (append saida (list (cons 101 (list media))))))
					(if (/= potLamp "") (setq saida (append saida (list (cons 103 (list potLamp))))))
					(if (= trafo 1) (setq saida (append saida (list (cons 106 (explode 32 (get_tile "posto")))))))
					(if (= pararaio 1) (setq saida (append saida (list (cons 104 (list "pararaio"))))))
					(if (= fusivel 1) (setq saida (append saida (list (cons 105 (list "chave-fusivel"))))))
					(if (= aterramento 1) (setq saida (append saida (list (cons 107 (list "aterramento"))))))
				)
				;-----------------------------------------------------------------------------------------------------------------------------------------------------
					  (set_tile "resistenciatamanho" (cadr (assoc 99 obj)))   
					  (set_tile "sequencia" (cadr (assoc 97 obj)))
					 
					  
					  
					  (if  (cdr (assoc 106 obj))
							(set_tile "posto" (strcat (cadr (assoc 106 obj)) " " (caddr (assoc 106 obj)) " " (cadddr (assoc 106 obj))))
							(set_tile "posto" " ") 
					  )
					  
					  (if  (cadr (assoc 103 obj))
							(set_tile "iluminacao" (cadr (assoc 103 obj))) 
							(set_tile "iluminacao" "") 
					  )
						
					  (if (assoc 100 obj)
							 (set_tile "estruturaMT" (cadr (assoc 101 obj)))
							 (set_tile "estruturaMT" "")
					  )
					  
					  (if (assoc 101 obj)
							 (set_tile "estruturaBT" (cadr (assoc 100 obj)))
							 (set_tile "estruturaBT" "")
					  )
					  
					  (if (assoc 107 obj)
							(set_tile "aterramento" "1")
							(set_tile "aterramento" "0")
					  )
					  
					  (if (assoc 104 obj)
							(set_tile "pararaio" "1")
							(set_tile "pararaio" "0")
					  )
					  
					  (if (assoc 105 obj)
							(set_tile "fusivel" "1")
							(set_tile "fusivel" "0")
					  )
					  
					  (if (assoc 106 obj)
							(set_tile "trafo" "1")
							(set_tile "trafo" "0")
					  )

					  
					  (if (= poste "dt")
							(set_tile "dt" "1")
					  )
					  (if (= poste "cc")
							(set_tile "cc" "1")
					  )
					  
					  (setq ddiag(start_dialog)) 
					  (unload_dialog dcl_id)
					  
					  (if (= ddiag 1)
						(progn
							(princ "\nsaindo...")
							(exit)
						)						
					  )
					  (if (= ddiag 2)
						(progn
							(princ "\nColocar ponto")
						)						
					 )
				)
			)
		)
	)	
	saida
)

;=================================================================================
(defun insereLuminaria (EI adesenhar lastposte / ID layeratual potencia osnapvar ponto)
	(if (setq potencia (cadr(assoc 103 adesenhar)))
		(progn
			(setq poste (cadr(assoc 98 adesenhar)))
			(setq ID (cadr(assoc 97 adesenhar)))
			(setq layeratual (getvar "clayer"))
			(setq osnapvar (getvar "osmode"))
			(setvar "osmode" 0)
			(setq ponto (cdr(assoc 10 (entget lastposte))))
			(if (= EI 1)
				(progn ; a instalar
					(setlayer g_layerPontosInst)			
					(command "_insert" g_BiluminacaoInst ponto "" "" "" potencia ID)
				)
				(progn ; existente
					(setlayer g_layerPostes)			
					(command "_insert" g_BiluminacaoExis ponto "" "" "" potencia ID)
				)
			)			
			(setq  lastLuminaria (entlast))
			(setvar "osmode" osnapvar)
			(setvar "clayer" layeratual)
			(colocariluminacao lastLuminaria lastposte poste)
		)
	)
	lastLuminaria
)
;=======================================================================================
;;;======================================================================================================================================================================================================
;funcao pra nserir uma luminaria ao poste
(defun colocariluminacao(luminaria bloco tipo / ang ponto grr pontoref numpi angulofinal anguloposte)
	(setq numpi 3.14159265359)
	(if (= tipo "dt")
	(progn
			(setq anguloposte  (cdr (assoc 50 (entget bloco))))
			(setq ang anguloposte)
			(if (> anguloposte (* numpi (/ 3.0 2.0)))
				(progn 
					(setq ang (- anguloposte  (* numpi (/ 3.0 2.0))))
				)
				(progn
					(if (> anguloposte (* numpi (/ 2.0 2.0)))
						(progn
							(setq ang (- anguloposte  (* numpi (/ 2.0 2.0))))
						)		
						(progn
							(if (> anguloposte (* numpi (/ 1.0 2.0)))
								(setq ang (- anguloposte  (* numpi (/ 1.0 2.0))))
							)
						)
					)
				)
			)

			(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
				(setq ponto (cadr grr))
				(setq pontoref (cdr (assoc 10 (entget luminaria))))
				(setq ponto (diferencaPontos pontoref ponto))
				(setq ponto (rotVetor ponto (* -1 (+ ang (* numpi (/ 1.0 4.0))))))
				(setq angulofinal 0)
				(if (and (> 0 (car ponto)) (> 0 (cadr ponto)))
					(progn
						(setq angulofinal (+ ang(* numpi (/ 0.0 4.0))))
					)
					(progn
						(if (and (< 0 (car ponto)) (> 0 (cadr ponto)))
							(progn
								(setq angulofinal (+ ang(* numpi (/ 2.0 4.0))))
							)
							(progn
								(if (and (< 0 (car ponto)) (< 0 (cadr ponto)))
									(progn
										(setq angulofinal (+ ang(* numpi (/ 4.0 4.0))))
									)
									(progn
										(if (and (> 0 (car ponto)) (< 0 (cadr ponto)))
											(progn
												(setq angulofinal (+ ang(* numpi (/ 6.0 4.0))))
											)
										)
									)
								)
								
							)
						)	
					)
				)
				(redraw)
				(girarTudo luminaria (+ angulofinal (* numpi (/ 2.0 4.0))))
				
			)
			
		)
		(progn
			(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
				(setq ponto (cadr grr))
				(setq pontoref (cdr (assoc 10 (entget luminaria))))
				(setq ponto (diferencaPontos pontoref ponto))
				(redraw)
				(girarTudo Luminaria (+ (atan (cadr ponto) (car ponto)) numpi)) 
			)
		)
	)
)

;;;======================================================================================================================================================================================================
;funcao para inserir uma chavefusivel em um poste
;essa rotina tem duas partes, definir o ponto de incerssao e o angulo da incerssao
;para a primeir parte temos duas situacoes, colocar a chave em poste dt e em cc
(defun inserechave (EI adesenhar lastposte / numpi saida layeratual ID incercaoPoste tipoPoste vetor ponto pontoref)
	;recentemente descobri que pi � uma variavel global, ou seja na presizamos da linha abaixo
	;mas apaga-la implica em mudar algumas linhas de codigo, portanto nao vou fazer isso agr
	(setq numpi 3.14159265359)
	
	(if (cdr(assoc 105 adesenhar));primeiro verificar se � necessario inserir a chave
		(progn
			(setq tipoPoste (cadr (assoc 98 adesenhar))); pegar o tipo de poste, cc ou dt
			(setq incercaoPoste (cdr (assoc 10 (entget lastposte)))); pegar o ponto de incesao do poste
			;pegar o vetor que vai do ponto de incessao ao indetificador do poste, o indetifiador tambem marca a posicao do que nao tem cava
			(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastposte)))) incercaoPoste)) 
			(setq ID (cadr(assoc 97 adesenhar)));pegar o indetificador do ponto, vou usar pra indentificar a chave
			
			(setq anguloInicial (atan (cadr vetor) (car vetor))); salvar o angulo inicial do vetor; esse angulo tambem representa o angulo do poste
			; pegar o modulo do vetor que indica a posicao do indentificador em relacao ao poste
			(setq modulo (sqrt(+ (* (car vetor) (car vetor)) (* (cadr vetor) (cadr vetor)))))
			
			(setq layeratual (getvar "clayer"));guardar o layer atual, vou mudar pro layer dos acessorios e depois voltar pro layer atual
			(if (= EI 1)
				(progn ; a instalar
					(setlayer g_layerPontosInst)			
					(command "_insert" g_BchaveFusivelInst (somaPontos incercaoPoste vetor) "" "" "" ID)
				)
				(progn ; existente
					(setlayer g_LayerAcessorios)			
					(command "_insert" g_BchaveFusivelExis (somaPontos incercaoPoste vetor) "" "" "" ID)
				)
			)
			;votar pro layer anterior
			(setvar "clayer" layeratual)
			(setq saida (entlast));adicionar o nome da ultima entidade inserida 'chavefusivel' a variavel saida
			
			;a primeira situacao representada � a de um poste DT, nele defini que existem quatro locais possives para colocar a chave
			;que � o centro de cada linha que compoe o lado do poste
			;para chegar nesses pontos estou rotacionando o vetor 'vetor' de em 90 graus apartir da posicao do mouse			
			(if (= tipoPoste "dt")
				(progn ;poste duplo t
					(while (= 5 (car (setq grr (grread t 13 0))));enquanto o mouse �  apenas movido, qualquer acao diferente dessa define a posicao final
						(setq ponto (cadr grr));pegar a localizacao do mouse
						(setq vetorMovimento (diferencaPontos ponto incercaoPoste)); pegar a posicao do mouse em relacao ao ponto de ince�ao do poste
						(setq vetorMovimento (rotVetor vetorMovimento (+ (* -1 anguloInicial) (* numpi 0.25))));subtrair o angulo inicial do vetor 
						;subtrai a posicao inicial pra poder usar a divisao de quadrantes do plano crtesiano para saber qual � o angulo que devera ter o meu vetor final
						(if (and (> 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
							(progn
								(setq novoAngulo (* numpi 0));primeiro quadrante
							)
							(progn
								(if (and (< 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
									(progn
										(setq novoAngulo (* numpi 0.5)); segundo quadrante
									)
									(progn
										(if (and (< 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
											(progn
												(setq novoAngulo (* numpi 1));terceiro quadrante
											)
											(progn
												(if (and (> 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
													(progn
														(setq novoAngulo (* numpi 1.5)); quarto quadrante
													)
												)
											)
										)
										
									)
								)	
							)
						)
						;ja tenho o modulo e o angulo do vetor que mostra a posicao da chave em relacao ao ponto de incersao do poste
						(setq vetor (polarParaCart (list modulo (+ anguloInicial novoAngulo (* numpi 1))))); passar pra cartesiano
						
						(redraw);atualizar o desenho
						(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada
					)
				)
				;pra circular �e mais facil, pq a chave pode ser inserida em qualquer angulo em relacao ao poste
				(progn ;poste circular
					(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o cursor se movimenta
						(setq ponto (cadr grr)); coordenadas do ponteiro do mouse
						;vetor que mostra a posicao do mouse em relacao ao poste
						(setq vetorMovimento (diferencaPontos ponto incercaoPoste))
						;utilizar o modulo do ponot de incerssao do indentificador do posete e o angulo do mouse
						(setq vetor (polarParaCart (list modulo (atan (cadr vetorMovimento) (car vetorMovimento)))))
						(redraw);atualizar o desenho
						(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada acima
					)			
				)		
			)
			;ja definida o ponto de incesao, calcular o angulo de rotacao da chave
			(princ "\nAngulo: ")
			(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o mousse se move
				(setq ponto (cadr grr)); coordenada do mouse
				(setq pontoref (cdr (assoc 10 (entget saida))));ponto em que esta o indentificador da chave
				(setq ponto (diferencaPontos pontoref ponto)); posicao do mouse em lrelacao a incerssao da chave
				(setq novoAngulo (atan (cadr ponto) (car ponto))); novo angulo da chave igual ao angulo do mouse
				;aqui sugestao do Tiago Manfrrim, inverter a chave pra ela nao ficar de cabeca para baixo no desenho
				(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
					(progn ;se o angulo passar de 90graus inverter a chave
						(mirrorTudo saida -1.0)
					)
					(progn
						(mirrorTudo saida 1.0)
						(setq novoAngulo (+ novoAngulo numpi))
					)					
				)
				(redraw); atualiza a chave
				(girarTudo saida novoAngulo);gira a chave com o angulo calculado
			)
		)
	)
	saida ;retorna o nome de entidade da chave
)
;======================================================================================================================================================
;to meio com preguissa de explicar esse, mas ele funciona igual ao inserir chave, so que a diferenca � voce nao escolhe o ponto de incessao
;apenas o angulo de rotacao do bloco
(defun inseretrafo (Ei adesenhar lastchave lastposte / numpi saida layeratual ID incercaoPoste tipoPoste vetor ponto pontoref)
	(princ "\ninserindo trafo\n")
	(setq numpi 3.14159265359)
	(if (cdr(assoc 106 adesenhar))
		(progn
			(if lastchave
				(progn
					(setq incercaoChave (cdr (assoc 10 (entget lastchave))))
					(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastchave)))) incercaoChave))
					(setq ID (cadr(assoc 97 adesenhar)))
					
					(setq anguloInicial (atan (cadr vetor) (car vetor)))
					
					(setq layeratual (getvar "clayer"))
					
					(if (= EI 1)
						(progn ; a instalar
							(setlayer g_layerPontosInst)			
							(command "_insert" g_BtrafoInstalar (somaPontos incercaoChave vetor) "" "" "" ID)
						)
						(progn ; existente
							(setlayer g_LayerAcessorios)			
							(command "_insert" g_BtrafoExistente (somaPontos incercaoChave vetor) "" "" "" ID)
						)
					)
					
					(setvar "clayer" layeratual)
					(setq saida (entlast))
							

					(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
						(setq ponto (cadr grr))
						(setq pontoref (cdr (assoc 10 (entget saida))))
						(setq ponto (diferencaPontos pontoref ponto))
						(setq novoAngulo (atan (cadr ponto) (car ponto)))
						(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
							(progn
								(mirrorTudo saida -1.0)
							)
							(progn
								(mirrorTudo saida 1.0)
								(setq novoAngulo (+ novoAngulo numpi))
							)						
						)
						(redraw)
						(girarTudo saida novoAngulo)
					)	
				)
				(progn ;------------------------------------ caso nao tenha chave -----------------------------------------------------------------------
					(setq tipoPoste (cadr (assoc 98 adesenhar))); pegar o tipo de poste, cc ou dt
					(setq incercaoPoste (cdr (assoc 10 (entget lastposte)))); pegar o ponto de incesao do poste
					;pegar o vetor que vai do ponto de incessao ao indetificador do poste, o indetifiador tambem marca a posicao do que nao tem cava
					(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastposte)))) incercaoPoste)) 
					(setq ID (cadr(assoc 97 adesenhar)));pegar o indetificador do ponto, vou usar pra indentificar a chave
					
					(setq anguloInicial (atan (cadr vetor) (car vetor))); salvar o angulo inicial do vetor; esse angulo tambem representa o angulo do poste
					; pegar o modulo do vetor que indica a posicao do indentificador em relacao ao poste
					(setq modulo (sqrt(+ (* (car vetor) (car vetor)) (* (cadr vetor) (cadr vetor)))))
					
					(setq layeratual (getvar "clayer"));guardar o layer atual, vou mudar pro layer dos acessorios e depois voltar pro layer atual
					(if (= EI 1)
						(progn ; a instalar
							(setlayer g_layerPontosInst)			
							(command "_insert" g_BtrafoInstalar (somaPontos incercaoPoste vetor) "" "" "" ID)
						)
						(progn ; existente
							(setlayer g_LayerAcessorios)			
							(command "_insert" g_BtrafoExistente (somaPontos incercaoPoste vetor) "" "" "" ID)
						)
					)
					;votar pro layer anterior
					(setvar "clayer" layeratual)
					(setq saida (entlast));adicionar o nome da ultima entidade inserida 'chavefusivel' a variavel saida
					
					;a primeira situacao representada � a de um poste DT, nele defini que existem quatro locais possives para colocar a chave
					;que � o centro de cada linha que compoe o lado do poste
					;para chegar nesses pontos estou rotacionando o vetor 'vetor' de em 90 graus apartir da posicao do mouse			
					(if (= tipoPoste "dt")
						(progn ;poste duplo t
							(while (= 5 (car (setq grr (grread t 13 0))));enquanto o mouse �  apenas movido, qualquer acao diferente dessa define a posicao final
								(setq ponto (cadr grr));pegar a localizacao do mouse
								(setq vetorMovimento (diferencaPontos ponto incercaoPoste)); pegar a posicao do mouse em relacao ao ponto de ince�ao do poste
								(setq vetorMovimento (rotVetor vetorMovimento (+ (* -1 anguloInicial) (* numpi 0.25))));subtrair o angulo inicial do vetor 
								;subtrai a posicao inicial pra poder usar a divisao de quadrantes do plano crtesiano para saber qual � o angulo que devera ter o meu vetor final
								(if (and (> 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
									(progn
										(setq novoAngulo (* numpi 0));primeiro quadrante
									)
									(progn
										(if (and (< 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
											(progn
												(setq novoAngulo (* numpi 0.5)); segundo quadrante
											)
											(progn
												(if (and (< 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
													(progn
														(setq novoAngulo (* numpi 1));terceiro quadrante
													)
													(progn
														(if (and (> 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
															(progn
																(setq novoAngulo (* numpi 1.5)); quarto quadrante
															)
														)
													)
												)
												
											)
										)	
									)
								)
								;ja tenho o modulo e o angulo do vetor que mostra a posicao da chave em relacao ao ponto de incersao do poste
								(setq vetor (polarParaCart (list modulo (+ anguloInicial novoAngulo (* numpi 1))))); passar pra cartesiano
								
								(redraw);atualizar o desenho
								(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada
							)
						)
						;pra circular �e mais facil, pq a chave pode ser inserida em qualquer angulo em relacao ao poste
						(progn ;poste circular
							(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o cursor se movimenta
								(setq ponto (cadr grr)); coordenadas do ponteiro do mouse
								;vetor que mostra a posicao do mouse em relacao ao poste
								(setq vetorMovimento (diferencaPontos ponto incercaoPoste))
								;utilizar o modulo do ponot de incerssao do indentificador do posete e o angulo do mouse
								(setq vetor (polarParaCart (list modulo (atan (cadr vetorMovimento) (car vetorMovimento)))))
								(redraw);atualizar o desenho
								(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada acima
							)			
						)		
					)
					;ja definida o ponto de incesao, calcular o angulo de rotacao da chave
					(princ "\nAngulo: ")
					(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o mousse se move
						(setq ponto (cadr grr)); coordenada do mouse
						(setq pontoref (cdr (assoc 10 (entget saida))));ponto em que esta o indentificador da chave
						(setq ponto (diferencaPontos pontoref ponto)); posicao do mouse em lrelacao a incerssao da chave
						(setq novoAngulo (atan (cadr ponto) (car ponto))); novo angulo da chave igual ao angulo do mouse
						;aqui sugestao do Tiago Manfrrim, inverter a chave pra ela nao ficar de cabeca para baixo no desenho
						(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
							(progn ;se o angulo passar de 90graus inverter a chave
								(mirrorTudo saida -1.0)
							)
							(progn
								(mirrorTudo saida 1.0)
								(setq novoAngulo (+ novoAngulo numpi))
							)					
						)
						(redraw); atualiza a chave
						(girarTudo saida novoAngulo);gira a chave com o angulo calculado
					)
				)
			)
		)
	)
	saida
)
;======================================================================================================================================================
;to meio com preguissa de explicar esse, mas ele funciona igual ao inserir chave, so que a diferenca � voce nao escolhe o ponto de incessao
;apenas o angulo de rotacao do bloco
(defun inserepararaio (EI adesenhar lasttrafo lastchave lastposte / numpi saida layeratual ID incercaoPoste tipoPoste vetor ponto pontoref)
	(princ "\ninserindo pararaio\n")
	(setq numpi 3.14159265359)
	(if (cdr(assoc 104 adesenhar))
		(progn
			(if (or lastchave lasttrafo)
				(progn
					(if lasttrafo
						(setq lastchave lasttrafo)
					)
					(setq incercaoChave (cdr (assoc 10 (entget lastchave))))
					(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastchave)))) incercaoChave))
					(setq ID (cadr(assoc 97 adesenhar)))
					
					(setq anguloInicial (atan (cadr vetor) (car vetor)))
					
					(setq layeratual (getvar "clayer"))
					
					(if (= EI 1)
						(progn ; a instalar
							(setlayer g_layerPontosInst)			
							(command "_insert" g_BpararaioInstalar (somaPontos incercaoChave vetor) "" "" "" ID)
						)
						(progn ; existente
							(setlayer g_LayerAcessorios)			
							(command "_insert" g_BpararaioExistente (somaPontos incercaoChave vetor) "" "" "" ID)
						)
					)
					
					(setvar "clayer" layeratual)
					(setq saida (entlast))
							

					(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
						(setq ponto (cadr grr))
						(setq pontoref (cdr (assoc 10 (entget saida))))
						(setq ponto (diferencaPontos pontoref ponto))
						(setq novoAngulo (atan (cadr ponto) (car ponto)))
						(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
							(progn
								(mirrorTudo saida -1.0)
							)
							(progn
								(mirrorTudo saida 1.0)
								(setq novoAngulo (+ novoAngulo numpi))
							)						
						)
						(redraw)
						(girarTudo saida novoAngulo)
					)	
				)
				(progn ;------------------------------------ caso nao tenha chave -----------------------------------------------------------------------
					(setq tipoPoste (cadr (assoc 98 adesenhar))); pegar o tipo de poste, cc ou dt
					(setq incercaoPoste (cdr (assoc 10 (entget lastposte)))); pegar o ponto de incesao do poste
					;pegar o vetor que vai do ponto de incessao ao indetificador do poste, o indetifiador tambem marca a posicao do que nao tem cava
					(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastposte)))) incercaoPoste)) 
					(setq ID (cadr(assoc 97 adesenhar)));pegar o indetificador do ponto, vou usar pra indentificar a chave
					
					(setq anguloInicial (atan (cadr vetor) (car vetor))); salvar o angulo inicial do vetor; esse angulo tambem representa o angulo do poste
					; pegar o modulo do vetor que indica a posicao do indentificador em relacao ao poste
					(setq modulo (sqrt(+ (* (car vetor) (car vetor)) (* (cadr vetor) (cadr vetor)))))
					
					(setq layeratual (getvar "clayer"));guardar o layer atual, vou mudar pro layer dos acessorios e depois voltar pro layer atual
					(if (= EI 1)
						(progn ; a instalar
							(setlayer g_layerPontosInst)			
							(command "_insert" g_BpararaioInstalar (somaPontos incercaoPoste vetor) "" "" "" ID)
						)
						(progn ; existente
							(setlayer g_LayerAcessorios)			
							(command "_insert" g_BpararaioExistente (somaPontos incercaoPoste vetor) "" "" "" ID)
						)
					)
					;votar pro layer anterior
					(setvar "clayer" layeratual)
					(setq saida (entlast));adicionar o nome da ultima entidade inserida 'chavefusivel' a variavel saida
					
					;a primeira situacao representada � a de um poste DT, nele defini que existem quatro locais possives para colocar a chave
					;que � o centro de cada linha que compoe o lado do poste
					;para chegar nesses pontos estou rotacionando o vetor 'vetor' de em 90 graus apartir da posicao do mouse			
					(if (= tipoPoste "dt")
						(progn ;poste duplo t
							(while (= 5 (car (setq grr (grread t 13 0))));enquanto o mouse �  apenas movido, qualquer acao diferente dessa define a posicao final
								(setq ponto (cadr grr));pegar a localizacao do mouse
								(setq vetorMovimento (diferencaPontos ponto incercaoPoste)); pegar a posicao do mouse em relacao ao ponto de ince�ao do poste
								(setq vetorMovimento (rotVetor vetorMovimento (+ (* -1 anguloInicial) (* numpi 0.25))));subtrair o angulo inicial do vetor 
								;subtrai a posicao inicial pra poder usar a divisao de quadrantes do plano crtesiano para saber qual � o angulo que devera ter o meu vetor final
								(if (and (> 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
									(progn
										(setq novoAngulo (* numpi 0));primeiro quadrante
									)
									(progn
										(if (and (< 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
											(progn
												(setq novoAngulo (* numpi 0.5)); segundo quadrante
											)
											(progn
												(if (and (< 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
													(progn
														(setq novoAngulo (* numpi 1));terceiro quadrante
													)
													(progn
														(if (and (> 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
															(progn
																(setq novoAngulo (* numpi 1.5)); quarto quadrante
															)
														)
													)
												)
												
											)
										)	
									)
								)
								;ja tenho o modulo e o angulo do vetor que mostra a posicao da chave em relacao ao ponto de incersao do poste
								(setq vetor (polarParaCart (list modulo (+ anguloInicial novoAngulo (* numpi 1))))); passar pra cartesiano
								
								(redraw);atualizar o desenho
								(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada
							)
						)
						;pra circular �e mais facil, pq a chave pode ser inserida em qualquer angulo em relacao ao poste
						(progn ;poste circular
							(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o cursor se movimenta
								(setq ponto (cadr grr)); coordenadas do ponteiro do mouse
								;vetor que mostra a posicao do mouse em relacao ao poste
								(setq vetorMovimento (diferencaPontos ponto incercaoPoste))
								;utilizar o modulo do ponot de incerssao do indentificador do posete e o angulo do mouse
								(setq vetor (polarParaCart (list modulo (atan (cadr vetorMovimento) (car vetorMovimento)))))
								(redraw);atualizar o desenho
								(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada acima
							)			
						)		
					)
					;ja definida o ponto de incesao, calcular o angulo de rotacao da chave
					(princ "\nAngulo: ")
					(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o mousse se move
						(setq ponto (cadr grr)); coordenada do mouse
						(setq pontoref (cdr (assoc 10 (entget saida))));ponto em que esta o indentificador da chave
						(setq ponto (diferencaPontos pontoref ponto)); posicao do mouse em lrelacao a incerssao da chave
						(setq novoAngulo (atan (cadr ponto) (car ponto))); novo angulo da chave igual ao angulo do mouse
						;aqui sugestao do Tiago Manfrrim, inverter a chave pra ela nao ficar de cabeca para baixo no desenho
						(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
							(progn ;se o angulo passar de 90graus inverter a chave
								(mirrorTudo saida -1.0)
							)
							(progn
								(mirrorTudo saida 1.0)
								(setq novoAngulo (+ novoAngulo numpi))
							)					
						)
						(redraw); atualiza a chave
						(girarTudo saida novoAngulo);gira a chave com o angulo calculado
					)
				)
			)
		)
	)
	saida	
)
;======================================================================================================================================================
;to meio com preguissa de explicar esse, mas ele funciona igual ao inserir chave, so que a diferenca � voce nao escolhe o ponto de incessao
;apenas o angulo de rotacao do bloco
(defun insereaterramento (EI adesenhar lastpararaio lasttrafo lastchave lastposte / numpi saida layeratual ID incercaoPoste tipoPoste vetor ponto pontoref)
	(princ "\ninserindo aterramento\n")
	(setq numpi 3.14159265359)
	(if (cdr(assoc 107 adesenhar))
		(progn
			(if (or lastchave lasttrafo lastpararaio)
				(progn
					(if lastpararaio
						(progn (setq lastchave lastpararaio ))
						(progn (if lasttrafo
							(setq lastchave lasttrafo )
						))
					)
					(setq incercaoChave (cdr (assoc 10 (entget lastchave))))
					(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastchave)))) incercaoChave))
					(setq ID (cadr(assoc 97 adesenhar)))
					
					(setq anguloInicial (atan (cadr vetor) (car vetor)))
					
					(setq layeratual (getvar "clayer"))
					
					(if (= EI 1)
						(progn ; a instalar
							(setlayer g_layerPontosInst)			
							(command "_insert" g_BaterramentoInstalar (somaPontos incercaoChave vetor) "" "" "" ID)
						)
						(progn ; existente
							(setlayer g_LayerAcessorios)			
							(command "_insert" g_BaterramentoExistente (somaPontos incercaoChave vetor) "" "" "" ID)
						)
					)
					
					(setvar "clayer" layeratual)
					(setq saida (entlast))
							

					(while (= 5 (car (setq grr (grread t 13 0)))) ;; While the cursor is moved
						(setq ponto (cadr grr))
						(setq pontoref (cdr (assoc 10 (entget saida))))
						(setq ponto (diferencaPontos pontoref ponto))
						(setq novoAngulo (atan (cadr ponto) (car ponto)))
						(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
							(progn
								(mirrorTudo saida -1.0)
							)
							(progn
								(mirrorTudo saida 1.0)
								(setq novoAngulo (+ novoAngulo numpi))
							)						
						)
						(redraw)
						(girarTudo saida novoAngulo)
					)	
				)
				(progn ;------------------------------------ caso nao tenha chave -----------------------------------------------------------------------
					(setq tipoPoste (cadr (assoc 98 adesenhar))); pegar o tipo de poste, cc ou dt
					(setq incercaoPoste (cdr (assoc 10 (entget lastposte)))); pegar o ponto de incesao do poste
					;pegar o vetor que vai do ponto de incessao ao indetificador do poste, o indetifiador tambem marca a posicao do que nao tem cava
					(setq vetor (diferencaPontos (cdr(assoc 10 (entget(entnext lastposte)))) incercaoPoste)) 
					(setq ID (cadr(assoc 97 adesenhar)));pegar o indetificador do ponto, vou usar pra indentificar a chave
					
					(setq anguloInicial (atan (cadr vetor) (car vetor))); salvar o angulo inicial do vetor; esse angulo tambem representa o angulo do poste
					; pegar o modulo do vetor que indica a posicao do indentificador em relacao ao poste
					(setq modulo (sqrt(+ (* (car vetor) (car vetor)) (* (cadr vetor) (cadr vetor)))))
					
					(setq layeratual (getvar "clayer"));guardar o layer atual, vou mudar pro layer dos acessorios e depois voltar pro layer atual
					(if (= EI 1)
						(progn ; a instalar
							(setlayer g_layerPontosInst)			
							(command "_insert" g_BaterramentoInstalar (somaPontos incercaoPoste vetor) "" "" "" ID)
						)
						(progn ; existente
							(setlayer g_LayerAcessorios)			
							(command "_insert" g_BaterramentoExistente (somaPontos incercaoPoste vetor) "" "" "" ID)
						)
					)
					;votar pro layer anterior
					(setvar "clayer" layeratual)
					(setq saida (entlast));adicionar o nome da ultima entidade inserida 'chavefusivel' a variavel saida
					
					;a primeira situacao representada � a de um poste DT, nele defini que existem quatro locais possives para colocar a chave
					;que � o centro de cada linha que compoe o lado do poste
					;para chegar nesses pontos estou rotacionando o vetor 'vetor' de em 90 graus apartir da posicao do mouse			
					(if (= tipoPoste "dt")
						(progn ;poste duplo t
							(while (= 5 (car (setq grr (grread t 13 0))));enquanto o mouse �  apenas movido, qualquer acao diferente dessa define a posicao final
								(setq ponto (cadr grr));pegar a localizacao do mouse
								(setq vetorMovimento (diferencaPontos ponto incercaoPoste)); pegar a posicao do mouse em relacao ao ponto de ince�ao do poste
								(setq vetorMovimento (rotVetor vetorMovimento (+ (* -1 anguloInicial) (* numpi 0.25))));subtrair o angulo inicial do vetor 
								;subtrai a posicao inicial pra poder usar a divisao de quadrantes do plano crtesiano para saber qual � o angulo que devera ter o meu vetor final
								(if (and (> 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
									(progn
										(setq novoAngulo (* numpi 0));primeiro quadrante
									)
									(progn
										(if (and (< 0 (car vetorMovimento)) (> 0 (cadr vetorMovimento)))
											(progn
												(setq novoAngulo (* numpi 0.5)); segundo quadrante
											)
											(progn
												(if (and (< 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
													(progn
														(setq novoAngulo (* numpi 1));terceiro quadrante
													)
													(progn
														(if (and (> 0 (car vetorMovimento)) (< 0 (cadr vetorMovimento)))
															(progn
																(setq novoAngulo (* numpi 1.5)); quarto quadrante
															)
														)
													)
												)
												
											)
										)	
									)
								)
								;ja tenho o modulo e o angulo do vetor que mostra a posicao da chave em relacao ao ponto de incersao do poste
								(setq vetor (polarParaCart (list modulo (+ anguloInicial novoAngulo (* numpi 1))))); passar pra cartesiano
								
								(redraw);atualizar o desenho
								(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada
							)
						)
						;pra circular �e mais facil, pq a chave pode ser inserida em qualquer angulo em relacao ao poste
						(progn ;poste circular
							(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o cursor se movimenta
								(setq ponto (cadr grr)); coordenadas do ponteiro do mouse
								;vetor que mostra a posicao do mouse em relacao ao poste
								(setq vetorMovimento (diferencaPontos ponto incercaoPoste))
								;utilizar o modulo do ponot de incerssao do indentificador do posete e o angulo do mouse
								(setq vetor (polarParaCart (list modulo (atan (cadr vetorMovimento) (car vetorMovimento)))))
								(redraw);atualizar o desenho
								(movetudo saida (somaPontos incercaoPoste vetor));mover a chave para a posicao calculada acima
							)			
						)		
					)
					;ja definida o ponto de incesao, calcular o angulo de rotacao da chave
					(princ "\nAngulo: ")
					(while (= 5 (car (setq grr (grread t 13 0)))); enquanto o mousse se move
						(setq ponto (cadr grr)); coordenada do mouse
						(setq pontoref (cdr (assoc 10 (entget saida))));ponto em que esta o indentificador da chave
						(setq ponto (diferencaPontos pontoref ponto)); posicao do mouse em lrelacao a incerssao da chave
						(setq novoAngulo (atan (cadr ponto) (car ponto))); novo angulo da chave igual ao angulo do mouse
						;aqui sugestao do Tiago Manfrrim, inverter a chave pra ela nao ficar de cabeca para baixo no desenho
						(if (and (< (+ novoAngulo (* numpi 0.5)) (* numpi 1)) (> (+ novoAngulo (* numpi 0.5)) 0))
							(progn ;se o angulo passar de 90graus inverter a chave
								(mirrorTudo saida -1.0)
							)
							(progn
								(mirrorTudo saida 1.0)
								(setq novoAngulo (+ novoAngulo numpi))
							)					
						)
						(redraw); atualiza a chave
						(girarTudo saida novoAngulo);gira a chave com o angulo calculado
					)
				)
			)
		)
	)
	saida	
	
	
)