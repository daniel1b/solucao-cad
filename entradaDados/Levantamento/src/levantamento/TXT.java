
package levantamento;

import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class TXT {

    public static void escrever(String text, String nomeArq)      {    
    FileWriter arquivo; 
    boolean erro = false;
        try
        {
        arquivo = new FileWriter(new File(nomeArq));
        arquivo.write(text);
        arquivo.close();
        }   catch (IOException e) {  
                e.printStackTrace();
                erro =true;
        } 
        if(!erro)
            JOptionPane.showMessageDialog(null, "gravado com Sucesso", "Resultado export", JOptionPane.INFORMATION_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "Ocorreu um erro", "Erro na exportacao", JOptionPane.ERROR);
    }
    public static String getLocal(){
        String saida = "";
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("txt", "txt");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           saida = chooser.getSelectedFile().getPath();
        }
        return saida;
    }
}
