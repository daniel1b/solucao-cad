package levantamento;

public class Ponto {
    public int sequencia;
    public boolean tipoDT;
    public int tamanho;
    public int resistencia;
    public long numeroPoste;
    
    public int estrBaixa;
    public int estrAlta;
    public int potIluminacao;
    
    public String CodPosto;
    public boolean ChaveFusivel;
    public boolean ParaRaio;
    public boolean Trafo;
    public int PotTrafo;
    public boolean Aterramento;
   
    public Ponto(){
       sequencia=1;
       tipoDT=true;
       tamanho=0;
       resistencia=0;
       numeroPoste=0;

       estrBaixa = 0;
       estrAlta = 0;
       potIluminacao = 0;

       CodPosto = "";
       ChaveFusivel=true;
       ParaRaio=true;
       Trafo=true;
       PotTrafo=0;
       Aterramento=true;
    }
}
    

