
package earth;

import java.io.FileWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class TXT {

    public static void escrever(String text, String nomeArq)      {    
    FileWriter arquivo; 
    boolean erro = false;
        try
        {
        arquivo = new FileWriter(new File(nomeArq));
        arquivo.write(text);
        arquivo.close();
        }   catch (IOException e) {  
                e.printStackTrace();
                erro =true;
        } 
        if(!erro)
            JOptionPane.showMessageDialog(null, "gravado com Sucesso", "Resultado export", JOptionPane.INFORMATION_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "Ocorreu um erro", "Erro na exportacao", JOptionPane.ERROR);
    }
    public static String getLocal(){
        String saida = "";
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           saida = chooser.getSelectedFile().getPath();
        }
        return saida;
    }
    public static void copyto(String origem, String destino){
        try{
            File arquivoOrigem = new File(origem); 
            File arquivoDestino = new File(destino); 
            if (arquivoDestino.exists())  
                arquivoDestino.delete();  

            FileChannel sourceChannel = null;  
            FileChannel destinationChannel = null;  

            try {  
                sourceChannel = new FileInputStream(arquivoOrigem).getChannel();  
                destinationChannel = new FileOutputStream(arquivoDestino).getChannel();  
                sourceChannel.transferTo(0, sourceChannel.size(),  
                        destinationChannel);  
            } finally {  
                if (sourceChannel != null && sourceChannel.isOpen())  
                    sourceChannel.close();  
                if (destinationChannel != null && destinationChannel.isOpen())  
                    destinationChannel.close();  
           }  
        }
        catch(Exception e){           
        }         
    }
    public static String textoApos(char caracter, String frase){
        String saida="";
        frase = ' '+frase;
        int comeco =0;
        do{
            comeco = frase.indexOf(caracter,comeco);            
                 frase = frase.substring(comeco+1);
                 //JOptionPane.showMessageDialog(null, comeco + " " + frase.charAt(comeco) + " - " + frase);
        }while((frase.indexOf(caracter))!=-1);

        saida = frase;
        return saida;
    }
    
    public static void criaDiretorio(String novoDiretorio){  
        String nomeDiretorio = null;   
        try {       
             if (!new File(nomeDiretorio).exists()) { // Verifica se o diretório existe.   
                 (new File(nomeDiretorio)).mkdir();   // Cria o diretório   
             }   
        } catch (Exception ex) {   
             JOptionPane.showMessageDialog(null,"Err","Erro ao criar o diretório" + ex.toString(),JOptionPane.ERROR_MESSAGE);   
        }  
    } 
    
}
